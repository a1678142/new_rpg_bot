import event
import messageReply
import item
import equip
import random
import NEWRPG
import globals
import 晴陽森林事件

#公共事件
class 撈魚1(event.事件):
    def __init__(self):
        self.名稱 = "釣魚1"
    async def 執行事件(self):
        玩家 = self.玩家
        隨機數 = random.random()
        if(隨機數>=0 and 隨機數<=0.33):
            文本 = "你甚麼都沒撈到。"
        if(隨機數>0.33 and 隨機數<=0.66):
            文本 = "你撈到了一隻魚。"
            玩家.背包.增加物品(item.魚(),1)
        if(隨機數>0.66 and 隨機數<=1):
            文本 = "你撈到了一尾蝦。"
            玩家.背包.增加物品(item.蝦(),1)
        await messageReply.send(玩家.uid,文本)
        玩家.當前事件=None

class 烤東西(event.事件):
    def __init__(self):
        self.名稱 = "烤東西"
        self.上限數量 = 3
    async def 執行事件(self):
        玩家 = self.玩家
        文本 = "=====================================" + "\n"
        文本 += "你想要烤甚麼?。" + "\n"
        文本 += "這個營火好像還能夠烤 "+str(self.上限數量)+" 個東西\n"
        文本 += "=====================================" + "\n"
        文本 += "1:烤魚 -需要魚" + "\n"
        文本 += "2:烤蝦 -需要蝦" + "\n"
        文本 += "3.烤豬肉 -需要豬肉" + "\n"
        if(self.上限數量==3):
            文本 += "c.做其他選擇"+"\n"
        else:
            文本 += "c.離開營火" + "\n"
        文本 += "=====================================" + "\n"
        await messageReply.send(玩家.uid,文本)
    async def 處理事件(self,message):
        msg =message.content.lower()
        玩家 = self.玩家
        if(玩家.鎖):
            msg = None
        #烤魚
        if(msg=="1"):
            if(玩家.背包.持有該物品('魚')[0]):
                self.上限數量-=1
                玩家.背包.減少物品("魚",1)
                文本 = "你把魚放在營火上，慢慢的等待著他烤好。\n"
                隨機數 = random.random()
                if(隨機數>=0 and 隨機數<0.45):
                   文本+="烤好了，你烤出了一條不錯的魚。\n"
                   玩家.背包.增加物品(item.烤魚(),1)
                if(隨機數>=0.45 and 隨機數<0.75):
                   文本+="一隻野貓跑過來把你烤到一半的魚搶走了QQ。\n"
                if(隨機數>=0.75 and 隨機數<=1):
                   文本+="你不小心把魚烤焦變成焦炭了。\n"
                   玩家.背包.增加物品(item.焦炭(),2)
            else:
                文本="你又沒有魚，是想要烤甚麼魚。"
            if(self.上限數量==0):
                文本+="營火熄滅了\n"
                await messageReply.send(玩家.uid,文本)
                玩家.當前事件=None
                玩家.鎖 = False
            else:
                await messageReply.send(玩家.uid,文本)
                await 玩家.當前事件.執行事件()
                玩家.鎖 = False
        #烤蝦
        if(msg=="2"):
            if(玩家.背包.持有該物品('蝦')[0]):
                self.上限數量-=1
                玩家.背包.減少物品("蝦",1)
                文本 = "你把蝦放在營火上，慢慢的等待著他烤好。\n"
                隨機數 = random.random()
                if(隨機數>=0 and 隨機數<0.45):
                   文本+="烤蝦逐漸轉成鮮紅色，好像烤好了。\n"
                   玩家.背包.增加物品(item.烤蝦(),1)
                if(隨機數>=0.45 and 隨機數<0.75):
                   文本+="一隻野狗跑過來把你烤到一半的蝦搶走了QQ。\n"
                if(隨機數>=0.75 and 隨機數<=1):
                   文本+="你不小心把魚烤焦變成焦炭了。\n"
                   玩家.背包.增加物品(item.焦炭(),2)
            else:
                文本="你又沒有蝦，是想要烤甚麼蝦。"
            if(self.上限數量==0):
                文本+="營火熄滅了\n"
                await messageReply.send(玩家.uid,文本)
                玩家.當前事件=None
                玩家.鎖 = False
            else:
                await messageReply.send(玩家.uid,文本)
                await 玩家.當前事件.執行事件()
                玩家.鎖 = False
         #烤豬肉
        if(msg=="3"):
            if(玩家.背包.持有該物品('豬肉')[0]):
                self.上限數量-=1
                玩家.背包.減少物品("豬肉",1)
                文本 = "你把豬肉放在營火上，慢慢的等待著他烤好。\n"
                隨機數 = random.random()
                if(隨機數>=0 and 隨機數<0.45):
                   文本+="豬肉散發出了香味，看著油潤的光澤，應該是烤好了。\n"
                   玩家.背包.增加物品(item.烤豬肉(),1)
                if(隨機數>=0.45 and 隨機數<0.75):
                   文本+="一隻野豬跑過來把你烤到一半的豬肉搶走了QQ。\n"
                if(隨機數>=0.75 and 隨機數<=1):
                   文本+="你不小心把豬肉烤焦變成焦炭了。\n"
                   玩家.背包.增加物品(item.焦炭(),2)
            else:
                文本="你又沒有豬肉，是想要烤甚麼豬肉。"
            if(self.上限數量==0):
                文本+="營火熄滅了\n"
                await messageReply.send(玩家.uid,文本)
                玩家.當前事件=None
                玩家.鎖 = False
            else:
                await messageReply.send(玩家.uid,文本)
                await 玩家.當前事件.執行事件()
                玩家.鎖 = False
        if(msg=="c"):
            if(self.上限數量!=3):
                文本="你離開了營火\n"
                await messageReply.send(玩家.uid,文本)
                玩家.當前事件=None
                玩家.鎖 = False
            else:
                玩家.當前事件 = 晴陽森林事件.晴陽森林_找到營火()
                玩家.當前事件.玩家 = 玩家
                await 玩家.當前事件.執行事件()
                玩家.鎖 = False
                
            
           
            

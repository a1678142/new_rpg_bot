# coding: utf-8
import discord

from discord.ext import commands
import random
from random import sample
import item
import equip
import messageReply
import skill


async def 新傷害公式(攻擊者,目標,傷害類型,標籤):
    文本= ""
    命中 = True
    爆擊 = False
    傷害 = 0
    for a in 傷害類型:
        if(a[0]=="物理"):
            傷害+=傷害公式(攻擊者.物理攻擊[0]+攻擊者.物理攻擊[1],目標.物理防禦[0]+目標.物理防禦[1])*a[1]
        if(a[0]=="魔法"):
            傷害+=傷害公式(攻擊者.魔法攻擊[0]+攻擊者.魔法攻擊[1],目標.魔法防禦[0]+目標.魔法防禦[1])*a[1]
        if(a[0]=="火屬性"):
            傷害+=傷害公式(攻擊者.火屬性攻擊力[0]+攻擊者.火屬性攻擊力[1],目標.火屬性防禦力[0]+目標.火屬性防禦力[1])*a[1]
        if(a[0]=="冰屬性"):
            傷害+=傷害公式(攻擊者.冰屬性攻擊力[0]+攻擊者.冰屬性攻擊力[1],目標.冰屬性防禦力[0]+目標.冰屬性防禦力[1])*a[1]
        if(a[0]=="地屬性"):
            傷害+=傷害公式(攻擊者.地屬性攻擊力[0]+攻擊者.地屬性攻擊力[1],目標.地屬性防禦力[0]+目標.地屬性防禦力[1])*a[1]
        #火傷
        if(a[0]=="火傷"):
            傷害+=傷害公式(a[1],目標.火屬性防禦力[0]+目標.火屬性防禦力[1])
        if(a[0]=="流血"):
            傷害+=傷害公式(a[1],目標.物理防禦[0]+目標.物理防禦[1])
        if(a[0]=="絕對"):
            傷害+=a[1]
        if(a[0]=="命中"):
            命中 = 命中公式(((攻擊者.命中[0]+攻擊者.命中[1])*a[1]),目標.閃避[0]+目標.閃避[1])
        if(a[0]=="爆擊"):
            爆擊值 = (攻擊者.爆擊[0]+攻擊者.爆擊[1])*a[1]
            麻痺效果 = 目標.含有效果("麻痺")
            if(麻痺效果!=False):
                for 效果 in 麻痺效果:
                    爆擊值+=效果.等級
            爆擊 = 爆擊公式(爆擊值)
    #命中判斷
    if(命中==False):
        return -1,False,"";
    #暴擊判斷
    if(爆擊==True):
        傷害 = 傷害 * 攻擊者.爆擊倍率
    #攻擊效果判斷
    if('攻擊' in 標籤 and 攻擊者.裝備!=None):
        #狼牙棒 50%攻擊造成8層流血
        if(攻擊者.裝備.含有裝備("狼牙棒")):
            隨機數 = random.random()
            if(隨機數>0.5):
                流血效果 = 流血(8,2)
                目標.效果列表.append(流血效果)
                文本+= 目標.名稱 +"流血了\n"
        
    if(傷害<0):
        傷害=0
    傷害 = int(傷害)
 
    # 
    if(目標.戰場.戰鬥結束==False):
        目標.當前血量 -= 傷害
    if (目標.當前血量<=0):
        文本+= await 目標.死亡()
    else:
        #尖刺
        尖刺效果 =  目標.含有效果("尖刺")
        if("近戰" in 標籤 and "絕對" not in 標籤 and 尖刺效果 != False):
            尖刺傷害 = 0
            for a in 尖刺效果:
                尖刺傷害+=a.等級
                尖刺傷害/=100
                尖刺傷害 = 傷害*尖刺傷害
                尖刺傷害 = int(尖刺傷害)
            文本 += 目標.名稱 +" 的尖刺使 "+攻擊者.名稱+" 受到了 " + str(尖刺傷害) + " 點傷害\n"
            (a,b,c)  = await 新傷害公式(目標,攻擊者,[["絕對",尖刺傷害]],["絕對"])
            文本 += c
        #地鼠王挖洞
        if(type(目標)==地鼠王 and 目標.挖洞==False and 目標.當前血量<75):
            目標.挖洞==True
            文本 += "地鼠王又挖洞了，趕快把它找出來。\n"
            標記列表 = ["地鼠王","地鼠","炸藥","空"]
            random.shuffle(標記列表)
            for a in 標記列表:
                新地洞=地洞("地洞",a)
                新地洞.戰場 =目標.戰場
                新地洞.玩家 = 目標.玩家
                目標.戰場.怪物方.append(新地洞)
            目標.戰場.怪物方.remove(目標)
    return 傷害,爆擊,文本

def 傷害公式(攻擊,防禦):
    傷害 = 攻擊-0.33*防禦
    if(傷害<0):
        傷害=0
    return int(傷害)

def 命中公式(命中,閃避):
    if(命中+閃避==0):
        命中=1
    命中率 = 命中/(命中+閃避)
    return random.random()<=(命中率+0.2)

def 爆擊公式(爆擊):
    爆擊率 = 1-0.993**爆擊
    return (random.random()<=爆擊率)
        
class 背包:
    def __init__(self):
        self.內容物 = dict()
        self.物品訊息 = dict()
        self.戰鬥物品對應 = dict()
    def 增加物品(self,物品,數量=1):
        if(物品.類型[0]=="裝備"):
            self.內容物[物品.編號]=1
            self.物品訊息[物品.編號] = 物品
        else:
            if(物品.名稱 not in self.內容物):
                self.內容物[物品.名稱]=0
                self.物品訊息[物品.名稱] = 物品
            self.內容物[物品.名稱]+=數量
    def 減少物品(self,物品名稱,數量):
        if(物品名稱 in self.內容物):
            self.內容物[物品名稱]-=數量
            if(self.內容物[物品名稱]==0):
                self.內容物.pop(物品名稱)
    def 背包資訊(self):
        文本 = ""
        for k,v in self.內容物.items():
            物品名稱 = ""
            if("裝備" in self.物品訊息[k].類型):
                物品名稱 = self.物品訊息[k].名稱 + "#" + str(self.物品訊息[k].編號)
            else:
                物品名稱 = k
            文本+=" "+物品名稱+" X "+str(v)+"\n"
        return 文本
    def 持有該物品(self,物品名稱):
        if(物品名稱 in self.內容物):
            return self.內容物[物品名稱],self.物品訊息[物品名稱]
        elif(物品名稱.isdigit()):
            if(int(物品名稱) in self.內容物):
                return self.內容物[int(物品名稱)],self.物品訊息[int(物品名稱)]
            else:
                return False,False
        else:
            return False,False
    def 戰鬥物品資訊(self):
        self.戰鬥物品對應 = dict()
        計數 = 0
        文本 = ""
        for k,v in self.內容物.items():
            if("戰鬥" in self.物品訊息[k].類型):
                文本+=str(計數)+" , "+self.物品訊息[k].名稱 + ":" + str(self.物品訊息[k].說明)+"\n"
                self.戰鬥物品對應[計數]=self.物品訊息[k]
                計數+=1
        return 文本
class 裝備:
    def __init__(self,人物,玩家):
        self.人物 = 人物
        self.玩家 = 玩家
        self.裝備欄位 = ["左手武器","右手武器","頭部","衣服","手部","腿部","飾品"]
        self.裝備列表 = dict()
        for a in range(len(self.裝備欄位)):
           self.裝備列表[self.裝備欄位[a]]=None
    def 含有裝備(self,名稱):
        for a in range(len(self.裝備欄位)):
            if(self.裝備列表[self.裝備欄位[a]]!=None and self.裝備列表[self.裝備欄位[a]].名稱==名稱):
                return True
        return False
    def 裝備資訊(self):
        文本 = ""
        for a in range(len(self.裝備欄位)):
            if(self.裝備列表[self.裝備欄位[a]]==None):
                文本+= str(a) +"." + self.裝備欄位[a] +" : " +"無" + "\n"
            else: 
                文本+= str(a) +"." + self.裝備欄位[a] +" : " + self.裝備列表[self.裝備欄位[a]].名稱 +"#" + str(self.裝備列表[self.裝備欄位[a]].編號) +"\n"
        return 文本
    def 裝上裝備(self,裝備):
        文本 = ""
        if("單手武器" in 裝備.類型):
            if(self.裝備列表["左手武器"]==None):
                self.玩家.背包.減少物品(裝備.編號,1)
                文本 += self.裝上裝備2(裝備,"左手武器")
            elif(self.裝備列表["右手武器"]==None and "雙手武器" not in self.裝備列表["左手武器"].類型):
                self.玩家.背包.減少物品(裝備.編號,1)
                文本 += self.裝上裝備2(裝備,"右手武器")
            else:
                文本 += "你需要先卸下武器，才能裝備該武器\n"
        if("雙手武器" in 裝備.類型):
            if(self.裝備列表["左手武器"]==None and self.裝備列表["右手武器"]==None):
                self.玩家.背包.減少物品(裝備.編號,1)
                文本 += self.裝上裝備2(裝備,"左手武器")
            else:
                文本 += "你需要先卸下雙手的武器，才能裝備該武器\n"
        if("衣服" in 裝備.類型):
            if(self.裝備列表["衣服"]==None):
                self.玩家.背包.減少物品(裝備.編號,1)
                文本 += self.裝上裝備2(裝備,"衣服")
            else:
                self.玩家.背包.減少物品(裝備.編號,1)
                文本 += self.卸下裝備("衣服")
                文本 += self.裝上裝備2(裝備,"衣服")
        if("手部" in 裝備.類型):
            if(self.裝備列表["手部"]==None):
                self.玩家.背包.減少物品(裝備.編號,1)
                文本 += self.裝上裝備2(裝備,"手部")
            else:
                self.玩家.背包.減少物品(裝備.編號,1)
                文本 += self.卸下裝備("手部")
                文本 += self.裝上裝備2(裝備,"手部")
        if("頭部" in 裝備.類型):
            if(self.裝備列表["頭部"]==None):
                self.玩家.背包.減少物品(裝備.編號,1)
                文本 += self.裝上裝備2(裝備,"頭部")
            else:
                self.玩家.背包.減少物品(裝備.編號,1)
                文本 += self.卸下裝備("頭部")
                文本 += self.裝上裝備2(裝備,"頭部")
        if("腿部" in 裝備.類型):
            if(self.裝備列表["腿部"]==None):
                self.玩家.背包.減少物品(裝備.編號,1)
                文本 += self.裝上裝備2(裝備,"腿部")
            else:
                self.玩家.背包.減少物品(裝備.編號,1)
                文本 += self.卸下裝備("腿部")
                文本 += self.裝上裝備2(裝備,"腿部")
        print(str(self.人物)+"裝上"+str(裝備))
        return 文本
    def 裝上裝備2(self,裝備,位置):
        self.裝備列表[位置]=裝備
        #
        self.人物.物理攻擊[0]+=裝備.物理攻擊
        self.人物.命中[0]+=裝備.命中
        self.人物.爆擊[0]+=裝備.爆擊
        self.人物.閃避[0]+=裝備.閃避
        self.人物.速度[0]+=裝備.速度
        self.人物.血量上限+=裝備.血量上限
        self.人物.物理防禦[0]+=裝備.物理防禦
        self.人物.魔法防禦[0]+=裝備.魔法防禦
        self.人物.冰屬性防禦力[0]+=裝備.冰屬性防禦力
        #
        userid = self.玩家.uid
        tag = "<@"+str(userid)+">"
        文本 = "於" + str(位置) + " 裝上了 " + 裝備.名稱 +"\n"
        return tag + 文本
    def 卸下裝備(self,位置):
        裝備 = self.裝備列表[位置]
        userid = self.玩家.uid
        tag = "<@"+str(userid)+">"
        if(裝備 == None):
            文本 = "無裝備可卸下"
        else:
            self.裝備列表[位置] = None
            #
            self.人物.物理攻擊[0]-=裝備.物理攻擊
            self.人物.命中[0]-=裝備.命中
            self.人物.爆擊[0]-=裝備.爆擊
            self.人物.閃避[0]-=裝備.閃避
            self.人物.速度[0]-=裝備.速度
            self.人物.血量上限-=裝備.血量上限
            self.人物.物理防禦[0]-=裝備.物理防禦
            self.人物.魔法防禦[0]-=裝備.魔法防禦
            self.人物.冰屬性防禦力[0]-=裝備.冰屬性防禦力
            #
            文本 = "於" + str(位置) + " 卸下了 " + 裝備.名稱 +"\n"
            self.玩家.背包.增加物品(裝備,1)
        return tag + 文本
        
class 玩家:
    def __init__(self):
        self.uid = None
        self.名稱 = None
        self.人物 = None
        self.位置 = [0,0]
        self.背包 = 背包()
        self.當前事件 = None
        self.鎖 = False
        self.金幣 = 0
        self.戰場 = None
        self.霜雪山丘_狀態 = 0
        self.霜雪山丘_討伐雪原狼任務布告欄 = 0
        self.霜雪山丘_雪人傳說 = 0
    def 獲得金幣(self,數量):
        self.金幣+=數量
        
    
狀態機 = 0
def 等待輸入():
    global 狀態機
    輸入 = input()
    if("a" in 輸入 and 狀態機 == 0):
        print("請選擇攻擊目標")
        for a in range(len(F.怪物方)):
            print(a,F.怪物方[a].名稱)
        狀態機=1
        等待輸入()
    elif(狀態機 == 1):
        x = int(輸入)
        玩家人物.攻擊(F.怪物方[x])
        狀態機 = 0
        玩家人物.行動結束()
    else:
        等待輸入()

        
class 生物:
    def __init__(self):
        self.名稱 = ""
        self.裝備 = None
        self.物理攻擊 = [0,0]
        self.物理防禦 = [0,0]
        self.速度 = [0,0]
        self.命中 = [0,0]
        self.爆擊 = [0,0]
        self.閃避 = [0,0]
        self.魔法攻擊 = [0,0]
        self.魔法防禦 = [0,0]
        self.爆擊倍率 = 0
        self.血量上限 = 0
        self.當前血量 = 0
        self.魔力上限 = 0
        self.當前魔力 = 0
        self.雷屬性攻擊力 = [0,0]
        self.火屬性攻擊力 = [0,0]
        self.冰屬性攻擊力 = [0,0]
        self.地屬性攻擊力 = [0,0]
        self.光屬性攻擊力 = [0,0]
        self.暗屬性攻擊力 = [0,0] 
        self.雷屬性防禦力 = [0,0]
        self.火屬性防禦力 = [0,0]
        self.冰屬性防禦力 = [0,0]
        self.地屬性防禦力 = [0,0]
        self.光屬性防禦力 = [0,0]
        self.暗屬性防禦力 = [0,0]
        self.是玩家 = False
        self.存活 = True
        #
        self.已行動 = False
        self.骰子列表 = list()
        self.效果列表 = list()
        #
        self.技能列表 = list()
        self.當前使用技能 =None
        #
        self.掉落物列表 = list()
        self.掉落機率 = list() #PDF
        self.掉落金錢 = 0
        self.擊殺經驗 = 0
        #
        self.行為 = None
        #
        self.水 = 0
        self.耗水強化 = False
        self.再次行動 = False
        #
        self.玩家 = False
        self.戰場 = None
    def 含有該技能(self,技能名稱):
        for 技能 in self.技能列表:
            if(技能名稱 == 技能.名稱):
                return 
        return False
    def 含有效果(self,效果名稱):
        表 = list()
        for 效果 in self.效果列表:
            if(效果名稱 == 效果.名稱):
                表.append(效果)
        if(len(表)==0):
            return False
        else:
            return 表
        
    async def 生物擲骰(self):
        生物 = self
        文本 = ""
        for 骰子 in self.骰子列表:
            點數 = 骰子.擲骰()
            內容 = 骰子.內容[點數]
            #print(生物.名稱+" 擲出了 "+內容)
            文本 += 生物.名稱+" 擲出了 "+內容+"\n"
            self.行為= ""
            if(內容.startswith("行動")):
                self.行為=內容
            效果 = 骰子.效果[點數]
            if(效果!=None):
                self.效果列表.append(效果)
                效果.獲得效果(生物)
            #飛鳥骰子
            if(內容=="飛行"):
                骰子.內容 = 骰子.空內容
                骰子.效果 = 骰子.空效果
            if(內容=="降落"):
                骰子.內容 = 骰子.陸內容
                骰子.效果 = 骰子.陸效果
            #落石骰子
            if(內容=="落石"):
                文本 += 生物.名稱+" 被落石砸中受到了 "+str(骰子.傷害)+" 點傷害\n"
                文本 += await self.受到傷害(骰子.傷害,self)
            if(內容=="迅捷"):
                文本 += 生物.名稱+" 發動了迅捷，立刻進行一次攻擊。\n"
                print(生物.名稱)
                if(self in self.戰場.玩家方):
                    攻擊目標 = sample(list(filter(lambda x:x.存活,self.戰場.怪物方)),1)[0]
                else:
                    攻擊目標 = sample(list(filter(lambda x:x.存活,self.戰場.玩家方)),1)[0]
                if(攻擊目標):
                    文本 += await self.攻擊(攻擊目標)
                    #await messageReply.send(self.玩家.uid,文本)
        return 文本
    async def 攻擊(self,目標):
        文本 = ""
        (傷害,爆擊,傷害文本) = await 新傷害公式(self,目標,[["物理",1.0],['命中',1.0],['爆擊',1.0]],['近戰','攻擊'])
        if(傷害!=-1):
            if(爆擊):
                文本 += self.名稱+" 對 "+目標.名稱+" 發動攻擊，造成了 "+str(傷害) +" 點傷害。爆擊\n"
            else:
                文本 += self.名稱+" 對 "+目標.名稱+" 發動攻擊，造成了 "+str(傷害) +" 點傷害。\n"
        else:
            文本 += self.名稱+" 對 "+目標.名稱+" 發動攻擊，但沒有命中\n"
        文本 += 傷害文本
        return 文本
    async def 行動(self,我方列表,敵方列表):
        self.戰場.玩家方 = list(filter(lambda x:x.存活,self.戰場.玩家方))
        self.戰場.怪物方 = list(filter(lambda x:x.存活,self.戰場.怪物方))
        print(str(self.名稱)+"行動")
        暈眩效果 = self.含有效果("暈眩") 
        if(暈眩效果 != False):
            self.已行動 = True
            self.當前使用技能= None
            文本 = self.名稱+"因為暈眩，而無法行動。"
            await messageReply.send(self.玩家.uid,文本)
            for a in 暈眩效果:
                a.持續回合數-=1
                if(a.持續回合數<=0):
                    a.失去效果(self)
            self.效果列表 = list(filter(lambda x:x.持續回合數>0 ,self.效果列表))
        
        #print(self.名稱,我方列表,敵方列表)
        return "this is abstract function"
    def 取得資訊(self):
        
        if(self.水!=0):
            文本 = "**名稱**:%s **血量**:%s/%s **魔力**:%s/%s **水**:%s **速度**:%s(+%s)\n" % (str(self.名稱),str(self.當前血量),str(self.血量上限),str(self.當前魔力),str(self.魔力上限),str(self.水),str(self.速度[0]),str(self.速度[1]))
        else:
            文本 = "**名稱**:%s **血量**:%s/%s **魔力**:%s/%s **速度**:%s(+%s)\n" % (str(self.名稱),str(self.當前血量),str(self.血量上限),str(self.當前魔力),str(self.魔力上限),str(self.速度[0]),str(self.速度[1]))
        #文本 = "**名稱**:{0} **血量**:{1}/{2} **速度**:{3}(+{4})\n".format(str(self.名稱),str(self.當前血量),str(self.血量上限),str(self.速度[0]),str(self.速度[1])) 
        return 文本
    
    def 受到不致死傷害(self,傷害量,來源):
        self.當前血量 -= 傷害量
        if (self.當前血量<=0):
            self.當前血量=1
        print(self.取得資訊())
        
    def 回復生命(self,回復量,來源):
        self.當前血量 += 回復量
        if (self.當前血量>self.血量上限):
            self.當前血量=self.血量上限
        print(self.取得資訊())
    async def 行動結束(self):
        self.戰場.玩家方 = list(filter(lambda x:x.存活,self.戰場.玩家方))
        self.戰場.怪物方 = list(filter(lambda x:x.存活,self.戰場.怪物方))
        if(self.再次行動):
            self.再次行動=False
            if(self.是玩家):
                await self.行動(self.戰場.玩家方,self.戰場.怪物方)
            else:
                await self.行動(self.戰場.怪物方,self.戰場.玩家方)
        else:
            #鬼火
            鬼火狀態  = self.含有效果("鬼火")
            if(鬼火狀態!=False):
                for a in range(鬼火狀態[0].等級):
                    if(self in self.戰場.玩家方):
                        目標 = sample(list(filter(lambda x:x.存活,self.戰場.怪物方)),1)[0]
                    else:
                        目標 = sample(list(filter(lambda x:x.存活,self.戰場.玩家方)),1)[0]
                    (傷害,爆擊,傷害文本)= await 新傷害公式(self,目標,[["火屬性",0.25]],['遠程'])
                    文本 = self.名稱+"身上的鬼火 對 "+目標.名稱+" 造成了 "+str(傷害) +" 點傷害。\n"
                    文本 += 傷害文本
                    await messageReply.send(self.玩家.uid,文本)
                    
            self.已行動 = True
            await self.戰場.戰鬥階段()
    async def 回合結束(self):
        for 骰子 in self.骰子列表:
            if(骰子.回合 != -1):
                骰子.回合 -= 1
        self.骰子列表 = list(filter(lambda x:x.回合!=0 ,self.骰子列表))
        for 效果 in self.效果列表:
        #燃燒
            if(效果.名稱=="燃燒"):
                (傷害,爆擊,傷害文本)= await 新傷害公式(self,self,[["火傷",效果.等級]],[])
                文本 = self.名稱+"因為身上著火而受到 "+str(傷害) +" 點傷害。\n"
                文本 +=傷害文本 
                await messageReply.send(self.玩家.uid,文本)
        #流血
            if(效果.名稱=="流血"):
                (傷害,爆擊,傷害文本)= await 新傷害公式(self,self,[["流血",效果.等級]],[])
                文本 = self.名稱+"因為身上流血而受到 "+str(傷害) +" 點傷害。\n"
                文本 +=傷害文本 
                await messageReply.send(self.玩家.uid,文本)        
            if(效果.衰退):
                效果.持續回合數-=1
            if(效果.持續回合數<=0):
                效果.失去效果(self)
        self.效果列表 = list(filter(lambda x:x.持續回合數>0 ,self.效果列表))
        for 技能 in self.技能列表:
            if(技能.當前冷卻!=0):
                技能.當前冷卻-=1
    async def 死亡(self):
        self.存活 = False
        print(self.名稱+"死亡")
        文本 = self.名稱+"死亡\n"
        #await messageReply.send(self.玩家.uid,文本)
        文本 += await self.戰場.戰場死亡處理(self)
        return 文本

    
    def 取得InfoEmbed(self):
        embed = discord.Embed(title="資訊", description=self.名稱, color=0x00ff00)
        embed.add_field(name="血量", value="{0}/{1}".format(str(self.當前血量),str(self.血量上限)), inline=True)
        embed.add_field(name="魔力", value="{0}/{1}".format(str(self.當前魔力),str(self.魔力上限)), inline=True)
        if(self.水!=0):
             embed.add_field(name="水", value="{0}".format(str(self.水)), inline=True)
        embed.add_field(name="速度", value="{0}(+{1})".format(str(self.速度[0]),str(self.速度[1])), inline=True)
     
        return embed
    def 取得詳細InfoEmbed(self):
        '''
        embed = discord.Embed(title="玩家資訊", description=self.名稱, color=0x00ff00)
        embed.add_field(name="血量", value="{0}/{1}".format(str(self.當前血量),str(self.血量上限)), inline=True)
        embed.add_field(name="魔力", value="{0}/{1}".format(str(self.當前魔力),str(self.魔力上限)), inline=True)
        if(self.水!=0):
            embed.add_field(name="水", value="{0}".format(str(self.水)), inline=True)
        embed.add_field(name="速度", value="{0}(+{1})".format(str(self.速度[0]),str(self.速度[1])), inline=True)
        embed.add_field(name="物理攻擊", value="{0}(+{1})".format(str(self.物理攻擊[0]),str(self.物理攻擊[1])), inline=True)
        embed.add_field(name="物理防禦", value="{0}(+{1})".format(str(self.物理防禦[0]),str(self.物理防禦[1])), inline=True)
        embed.add_field(name="魔法攻擊", value="{0}(+{1})".format(str(self.魔法攻擊[0]),str(self.魔法攻擊[1])), inline=True)
        embed.add_field(name="魔法防禦", value="{0}(+{1})".format(str(self.魔法防禦[0]),str(self.魔法防禦[1])), inline=True)
        embed.add_field(name="命中", value="{0}(+{1})".format(str(self.命中[0]),str(self.命中[1])), inline=True)
        embed.add_field(name="閃避", value="{0}(+{1})".format(str(self.閃避[0]),str(self.閃避[1])), inline=True)
        embed.add_field(name="爆擊", value="{0}(+{1})".format(str(self.爆擊[0]),str(self.爆擊[1])), inline=True)
        embed.add_field(name="屬性攻擊力", value="雷:{0}(+{1}) 火:{2}(+{3}) 冰:{4}(+{5}) 地:{6}(+{7}) 光:{8}(+{9}) 暗:{10}(+{11})".format(str(self.雷屬性攻擊力[0]),str(self.雷屬性攻擊力[1]),str(self.火屬性攻擊力[0]),str(self.火屬性攻擊力[1]),str(self.冰屬性攻擊力[0]),str(self.冰屬性攻擊力[1]),str(self.地屬性攻擊力[0]),str(self.地屬性攻擊力[1]),str(self.光屬性攻擊力[0]),str(self.光屬性攻擊力[1]),str(self.暗屬性攻擊力[0]),str(self.暗屬性攻擊力[1])), inline=True)
        embed.add_field(name="屬性防禦力", value="雷:{0}(+{1}) 火:{2}(+{3}) 冰:{4}(+{5}) 地:{6}(+{7}) 光:{8}(+{9}) 暗:{10}(+{11})".format(str(self.雷屬性防禦力[0]),str(self.雷屬性防禦力[1]),str(self.火屬性防禦力[0]),str(self.火屬性防禦力[1]),str(self.冰屬性防禦力[0]),str(self.冰屬性防禦力[1]),str(self.地屬性防禦力[0]),str(self.地屬性防禦力[1]),str(self.光屬性防禦力[0]),str(self.光屬性防禦力[1]),str(self.暗屬性防禦力[0]),str(self.暗屬性防禦力[1])), inline=True)
        for 狀態 in self.效果列表:
            文本 = ""
            文本+="等級:"+str(狀態.等級)
            if(狀態.衰退):
                 文本+= "剩餘回合:"+str(狀態.持續回合數)
            文本+="\n"
            embed.add_field(name=狀態.名稱,value=文本, inline=False)
            
        
        return embed
        '''
        if(self.水!=0):
            文本 = "**名稱**:%s **血量**:%s/%s **魔力**:%s/%s **水**:%s **速度**:%s(+%s)\n" % (str(self.名稱),str(self.當前血量),str(self.血量上限),str(self.當前魔力),str(self.魔力上限),str(self.水),str(self.速度[0]),str(self.速度[1]))
        else:
            文本 = "**名稱**:%s **血量**:%s/%s **魔力**:%s/%s **速度**:%s(+%s)\n" % (str(self.名稱),str(self.當前血量),str(self.血量上限),str(self.當前魔力),str(self.魔力上限),str(self.速度[0]),str(self.速度[1]))    
        if(len(self.效果列表)!=0):
            文本 += "**狀態**:\n"
            for 狀態 in self.效果列表:
                文本+= "["+狀態.名稱+"]"
                文本+=" 等級:"+str(狀態.等級)
                if(狀態.衰退):
                     文本+= " 剩餘回合:"+str(狀態.持續回合數)
                文本+="\n"
        return 文本
    async def 受到傷害(self,傷害量,來源):
        文本 = ""
        if(self.存活):
            self.當前血量 -= 傷害量
            embed = self.取得InfoEmbed()
            if (self.當前血量<=0):
                文本 += await self.死亡()
        #print(self.取得資訊())
        return 文本

    async def 使用技能(self,目標):
        self.當前使用技能.當前冷卻 = self.當前使用技能.冷卻回合數
        #重擊
        if(self.當前使用技能.名稱=="重擊"):
            (傷害,爆擊,傷害文本)= await 新傷害公式(self,目標,[["物理",1.5],['命中',1.0],['爆擊',1.0]],['近戰','攻擊'])
            if(傷害!=-1):
                文本 = self.名稱+" 對 "+目標.名稱+" 使出了重擊，造成了 "+str(傷害) +" 點傷害。"
                if(爆擊):
                    文本+="爆擊"
                文本+="\n"
                隨機數 = random.random()
                暈眩機率 = 傷害/目標.血量上限
                if(隨機數<暈眩機率):
                    文本+=目標.名稱+"被擊暈了。\n"
                    目標.效果列表.append(暈眩(1))       
                文本 += 傷害文本
            else:
                文本 = self.名稱+" 對 "+目標.名稱+" 使出了重擊，可惜沒有命中。\n"
            await messageReply.send(self.玩家.uid,文本)
            
        #漩渦
        if(self.當前使用技能.名稱=="漩渦"):
            文本 = "你使用了漩渦\n"
            await messageReply.send(self.玩家.uid,文本)
            文本 = ""
            for a in 目標:
                (傷害,爆擊,傷害文本)= await 新傷害公式(self,a,[["魔法",0.3],['冰屬性',0.8]],[])
                if(self.耗水強化):
                    文本 += self.名稱+" 對 "+a.名稱+" 造成了 "+str(傷害) +" 點傷害，並使其獲得4層冰緩，持續2回合。\n"
                    冰緩效果  = 冰緩(4,2)
                    a.效果列表.append(冰緩效果)
                    冰緩效果.獲得效果(a)
                else:
                    文本 += self.名稱+" 對 "+a.名稱+" 造成了 "+str(傷害) +" 點傷害。\n"
                文本 += 傷害文本
                self.當前使用技能.當前冷卻+=1
            await messageReply.send(self.玩家.uid,文本)
        #召喚雪人
        if(self.當前使用技能.名稱=="召喚雪人"):
            文本 = "你召喚出了兩隻雪人\n"
            await messageReply.send(self.玩家.uid,文本)
            雪人A = 雪人("雪人1號")
            雪人B = 雪人("雪人2號")
            雪人A.戰場=self.戰場
            雪人B.戰場=self.戰場
            雪人A.玩家=self.玩家
            雪人B.玩家=self.玩家
            目標.append(雪人A)
            目標.append(雪人B)
        #鬼火術
        if(self.當前使用技能.名稱=="鬼火術"):
            文本 = "一道鬼火圍繞在你的身旁\n"
            鬼火狀態 = self.含有效果("鬼火")
            if(鬼火狀態==False):
                鬼火狀態 = 鬼火()
                self.效果列表.append(鬼火狀態)
                鬼火狀態.獲得效果(self)
            elif(鬼火狀態[0].等級<4):
                鬼火狀態[0].等級+=1
                鬼火狀態[0].獲得效果(self)
            await messageReply.send(self.玩家.uid,文本)
        #火球術
        if(self.當前使用技能.名稱=="火球術"):
            (傷害,爆擊,傷害文本)= await 新傷害公式(self,目標,[["魔法",0.3],['火屬性',0.4]],[])
            文本 = self.名稱+" 對 "+目標.名稱+" 丟出了火球，造成了 "+str(傷害) +" 點火系魔法傷害。\n"
            文本+=傷害文本
            燃燒傷害 = int((self.火屬性攻擊力[0]+self.火屬性攻擊力[1])*0.4)
            燃燒效果 = 燃燒(燃燒傷害,3)
            目標.效果列表.append(燃燒效果)
            文本 += 目標.名稱+" 獲得了 "+str(燃燒傷害) +" 層燃燒效果，持續 3 回合。\n"
            await messageReply.send(self.玩家.uid,文本)
            
        #蔓延
        if(self.當前使用技能.名稱=="蔓延"):
            傷害 = 傷害公式((self.火屬性攻擊力[0]+self.火屬性攻擊力[1])*1.2,目標.火屬性防禦力[0]+目標.火屬性防禦力[1])
            傷害 = int(傷害)
            文本 = self.名稱+" 引燃了 "+目標.名稱+" 身上的火勢對其造成 "+str(傷害) +" 點火系傷害，並使得火勢蔓延了開來。\n"
            燃燒效果列表 = 目標.含有效果("燃燒")
            if(燃燒效果列表!=False):
                for 燃燒效果 in 燃燒效果列表:
                    for 新目標 in self.戰場.玩家方 + self.戰場.怪物方:
                        if 新目標 != 目標:
                            新燃燒效果 = 燃燒(燃燒效果.等級//2,燃燒效果.持續回合數)
                            新目標.效果列表.append(新燃燒效果)
                            文本+=新目標.名稱+" 獲得了 "+str(新燃燒效果.等級) +" 層燃燒效果，持續"+str(新燃燒效果.持續回合數)+"回合。\n"
            文本+=await 目標.受到傷害(傷害,self)
            await messageReply.send(self.玩家.uid,文本)
            
        #電火花
        if(self.當前使用技能.名稱=="電火花"):
            文本 = "你在手上捏出一簇火花\n"
            初級雷骰A = 初級雷骰()
            初級雷骰B = 初級雷骰()
            初級雷骰A.回合=3
            初級雷骰B.回合=3
            self.骰子列表.append(初級雷骰A)
            self.骰子列表.append(初級雷骰B)
        #電球術
        if(self.當前使用技能.名稱=="電球術"):
            文本 = "你從手中發出四顆電球\n"
            #await messageReply.send(self.玩家.uid,文本)
            for a in range(4):
                if(self in self.戰場.玩家方):
                    目標清單 = list(filter(lambda x:x.存活,self.戰場.怪物方))
                else:
                    目標清單 = list(filter(lambda x:x.存活,self.戰場.玩家方))
                if(len(目標清單)>0):
                    目標 = sample(目標清單,1)[0]
                else:
                    continue;
                傷害 = 傷害公式((self.魔法攻擊[0]+self.魔法攻擊[1])*0.2+(self.雷屬性攻擊力[0]+self.雷屬性攻擊力[1])*0.2,目標.魔法防禦[0]+目標.魔法防禦[1]+目標.雷屬性防禦力[0]+目標.雷屬性防禦力[1])
                傷害 = int(傷害)
                #麻痺
                爆擊值 = self.爆擊[0]+self.爆擊[1]
                麻痺效果 = 目標.含有效果("麻痺")
                if(麻痺效果!=False):
                    for 效果 in 麻痺效果:
                        爆擊值+=效果.等級
                爆擊 = 爆擊公式(爆擊值)
                if(爆擊):
                    傷害 = 傷害*self.爆擊倍率
                    傷害 = int(傷害)
                    文本 += self.名稱+"使用電球 對 "+目標.名稱+" 造成了 "+str(傷害) +" 點傷害。 爆擊\n"
                    #await messageReply.send(self.玩家.uid,文本)
                else:
                    文本 += self.名稱+"使用電球 對 "+目標.名稱+" 造成了 "+str(傷害) +" 點傷害。 \n"
                    #await messageReply.send(self.玩家.uid,文本)
                文本+= await 目標.受到傷害(傷害,self)
            await messageReply.send(self.玩家.uid,文本)
            
        #麻痺術
        if(self.當前使用技能.名稱=="麻痺術"):
            傷害 = 傷害公式((self.雷屬性攻擊力[0]+self.雷屬性攻擊力[1])*1.5,目標.雷屬性防禦力[0]+目標.雷屬性防禦力[1])
            傷害 = int(傷害)
            #麻痺
            爆擊值 = self.爆擊[0]+self.爆擊[1]
            麻痺效果 = 目標.含有效果("麻痺")
            if(麻痺效果!=False):
                for 效果 in 麻痺效果:
                    爆擊值+=效果.等級
            爆擊 = 爆擊公式(爆擊值)
            if(爆擊):
                傷害 = 傷害*self.爆擊倍率
                傷害 = int(傷害)
                文本 = self.名稱+" 對 "+目標.名稱+" 施放了麻痺術造成了 "+str(傷害) +" 點雷系傷害，並使其陷入了麻痺，持續3回合。 爆擊\n"
            else:
                文本 = self.名稱+" 對 "+目標.名稱+" 施放了麻痺術造成了 "+str(傷害) +" 點雷系傷害，並使其陷入了麻痺，持續3回合。\n"
            麻痺效果 = 麻痺(15,3)
            目標.效果列表.append(麻痺效果)
            文本 += await 目標.受到傷害(傷害,self)
            await messageReply.send(self.玩家.uid,文本)
           
        #落石
        if(self.當前使用技能.名稱=="落石"):
            傷害 = (self.魔法攻擊[0]+self.魔法攻擊[1])*0.3+(self.地屬性攻擊力[0]+self.地屬性攻擊力[1])*0.3
            傷害 = int(傷害)
            落石A = 落石骰子()
            落石B = 落石骰子()
            落石C = 落石骰子()
            落石A.回合=3
            落石B.回合=3
            落石C.回合=3
            落石A.傷害 = 傷害
            落石B.傷害 = 傷害
            落石C.傷害 = 傷害
            目標.骰子列表.append(落石A)
            目標.骰子列表.append(落石B)
            目標.骰子列表.append(落石C)
            文本 = "你召喚出了一些落石\n"
            await messageReply.send(self.玩家.uid,文本)
        #石化術
        if(self.當前使用技能.名稱=="石化術"):
            石化效果 =石化(4,3)
            目標.效果列表.append(石化效果)
            石化效果.獲得效果(目標)
            文本 = self.名稱+" 對 "+目標.名稱+" 施放了石化術\n"
            await messageReply.send(self.玩家.uid,文本)
        #小型地震
        if(self.當前使用技能.名稱=="小型地震"):
            文本 = self.名稱+" 對 "+目標.名稱+" 施放了小型地震\n"
            for 骰子 in 目標.骰子列表:
                if(骰子.效果[骰子.當前點數]!=None):
                    骰子.效果[骰子.當前點數].失去效果(目標)
            文本+= await 目標.生物擲骰()
            傷害 = 傷害公式((self.魔法攻擊[0]+self.魔法攻擊[1])*0.8+(self.地屬性攻擊力[0]+self.地屬性攻擊力[1])*0.6,目標.魔法防禦[0]+目標.魔法防禦[1]+目標.地屬性防禦力[0]+目標.地屬性防禦力[1])
            傷害 = int(傷害)
            文本 += "小型地震對 "+目標.名稱+" 造成了" +str(傷害) + " 點傷害\n"
            文本 +=  await 目標.受到傷害(傷害,self)
            await messageReply.send(self.玩家.uid,文本)
           
        #活水術
        if(self.當前使用技能.名稱=="活水術"):
            水數量 = 60
            if(self.耗水強化):
                水數量= 80
            文本 = self.名稱+" 施放了活水術製造出了" + str(水數量) + " 點水。\n"
            self.水+= 水數量
            await messageReply.send(self.玩家.uid,文本)
        #冰箭
        if(self.當前使用技能.名稱=="冰箭"):
            (傷害,爆擊,傷害文本)= await 新傷害公式(self,目標,[["魔法",0.6],['冰屬性',0.6]],['遠程'])
            文本 = self.名稱+" 對 "+目標.名稱+" 丟出了冰箭，造成了 "+str(傷害) +" 點冰系魔法傷害，並使其陷入冰緩。\n"
            冰緩效果  = 冰緩(3,3)
            目標.效果列表.append(冰緩效果)
            冰緩效果.獲得效果(目標)
            if(self.耗水強化):
                self.當前使用技能.當前冷卻=0
            文本 += 傷害文本
            await messageReply.send(self.玩家.uid,文本)
            
        #影襲
        if(self.當前使用技能.名稱=="影襲"):
            傷害 = 傷害公式((self.物理攻擊[0]+self.物理攻擊[1])*0.6+(self.暗屬性攻擊力[0]+self.暗屬性攻擊力[1])*0.6,目標.物理防禦[0]+目標.物理防禦[1]+目標.暗屬性防禦力[0]+目標.暗屬性防禦力[1])
            傷害 = int(傷害)
            文本 = self.名稱+" 對 "+目標.名稱+" 使出了影襲造成了 "+str(傷害) +" 點暗系物理傷害。\n"
            self.再次行動  = True
            文本 += await 目標.受到傷害(傷害,self)
            await messageReply.send(self.玩家.uid,文本)
            
        #怒吼
        if(self.當前使用技能.名稱=="怒吼"):
            文本 = self.名稱+" 怒吼了一聲。\n"
            
            if(self in self.戰場.玩家方):
                for 目標 in self.戰場.玩家方:
                    怒吼效果  = 攻擊增加效果(2,2)
                    目標.效果列表.append(怒吼效果)
                    怒吼效果.獲得效果(目標)
            else:
                for 目標 in self.戰場.怪物方:
                    怒吼效果  = 攻擊增加效果(2,2)
                    目標.效果列表.append(怒吼效果)
                    怒吼效果.獲得效果(目標)
            await messageReply.send(self.玩家.uid,文本)
        #霜寒吐息
        if(self.當前使用技能.名稱=="霜寒吐息"):
            文本 = self.名稱+" 對 "+目標.名稱+" 吐出了一口寒氣，附加了3層冰緩，持續15回合。你感覺到了一絲寒意，若是速度低於-30，你將直接死亡\n"
            冰緩效果  = 冰緩(3,15)
            目標.效果列表.append(冰緩效果)
            冰緩效果.獲得效果(目標)
            if(目標.速度[0]+目標.速度[1]<-30):
                文本 += "你已經無法承受更多的寒冷了。\n"
                (a,b,c)  = await 新傷害公式(目標,攻擊者,[["絕對",9999999]],["絕對"])
                文本 += c
            await messageReply.send(self.玩家.uid,文本)
        #憤怒蘑菇
        if(self.當前使用技能.名稱=="憤怒蘑菇"):
            if(self.含有效果("憤怒")==False):
                self.玩家.背包.減少物品("憤怒蘑菇",1)
                傷害 = (int)(self.血量上限*0.10)
                self.受到不致死傷害(傷害,None)
                數值 =int(self.物理攻擊[0]*0.15)
                數值 +=3
                蘑菇效果 = 憤怒(數值,1)
                self.效果列表.append(蘑菇效果)
                蘑菇效果.獲得效果(self)
                文本 = self.名稱+" 吞下了憤怒蘑菇，受到了 "+str(傷害)+" 點傷害，提高了 "+str(數值)+" 點物理攻擊力\n"
                self.再次行動  = True
                await messageReply.send(self.玩家.uid,文本)
            else:
                文本 = "你已經吃過憤怒蘑菇了\n"
                self.再次行動  = True
                await messageReply.send(self.玩家.uid,文本)
                
        self.當前使用技能 = None    

class 玩家角色(生物):
    def __init__(self,玩家):
        super(玩家角色, self).__init__()
        self.玩家 = 玩家
        self.裝備 = 裝備(self,玩家)
        self.名稱 ="ASDF"
        self.等級 = 1
        self.經驗 = 0
        self.升級所需經驗 = 10
        self.物理攻擊 = [10,0]
        #self.物理攻擊 = [60,0]
        self.物理防禦 = [0,0]
        self.速度 = [4,0]
        self.命中 = [10,0]
        #self.命中 = [500,0]
        self.爆擊 = [15,0]
        self.閃避 = [2,0]
        self.魔法攻擊 = [10,0]
        self.魔法防禦 = [0,0]
        self.爆擊倍率 = 1.5
        self.血量上限 = 40
        self.當前血量 = 40
        self.魔力上限 = 40
        self.當前魔力 = 40
        self.火屬性攻擊力 = [10,0]
        self.雷屬性攻擊力 = [10,0]
        self.地屬性攻擊力 = [10,0]
        self.冰屬性攻擊力 = [10,0]
        self.是玩家 = True
        self.可以逃跑 = True
        #
        self.技能列表 = [skill.重擊()]
        self.骰子列表 = [新手骰子_攻擊(),新手骰子_防禦()]
    async def 行動(self,我方列表,敵方列表):
        self.戰場.玩家方 = list(filter(lambda x:x.存活,self.戰場.玩家方))
        self.戰場.怪物方 = list(filter(lambda x:x.存活,self.戰場.怪物方))
        await super().行動(我方列表,敵方列表)
        if(self.已行動 == False):
            文本="輪到 "+ self.名稱 +" 行動了\n"
            文本+="輸入 a 進行攻擊\n"
            文本+="輸入 s 使用技能\n"
            文本+="輸入 b 使用物品\n"
            文本+="輸入 i 查看戰場詳細資訊\n"
            if self.可以逃跑:
                文本+="輸入 r 逃離戰鬥\n"
            await messageReply.send(self.玩家.uid,文本)
        else:
            await self.行動結束()
        #等待輸入()
    async def 獲得經驗(self,數量):
        文本 = self.名稱 + "獲得了 "+str(數量)+" 點經驗。"
        await messageReply.send(self.玩家.uid,文本)
        self.經驗+=數量
        if(self.經驗>=self.升級所需經驗):
            self.升級()
            文本 = self.名稱 + "升級了。"
            await messageReply.send(self.玩家.uid,文本)
    async def 取得技能資訊(self):
        文本 = "選擇要使用的技能\n"
        for a in range(len(self.技能列表)):
            文本+=str(a)+":"+self.技能列表[a].名稱+"("+str(self.技能列表[a].當前冷卻)+") 耗魔:"+str(self.技能列表[a].消耗魔力)+":"+self.技能列表[a].描述+"\n\n"
        文本+="c:取消"
        await messageReply.send(self.玩家.uid,文本)
    async def 取得戰鬥物品資訊(self):
        文本 = "選擇要使用的物品\n"
        文本 +=self.玩家.背包.戰鬥物品資訊()
        文本 +="c:取消"
        await messageReply.send(self.玩家.uid,文本)
    def 升級(self):
        #TODO 顯示升級訊息
        self.經驗-=self.升級所需經驗
        self.等級+=1
        #升級係數
        self.物理攻擊[0]+=1
        self.魔法攻擊[0]+=1
        self.閃避[0]+=1
        self.命中[0]+=1
        self.爆擊[0]+=1
        self.血量上限+=2
        self.當前血量 = self.血量上限
        self.升級所需經驗+=10
class 野豬(生物):
    def __init__(self,名稱):
        super(野豬, self).__init__()
        self.名稱 = 名稱
        self.物理攻擊 = [5,0]
        self.物理防禦 = [4,0]
        self.速度 = [4,0]
        self.命中 = [8,0]
        self.爆擊 = [0,0]
        self.閃避 = [0,0]
        self.魔法攻擊 = [0,0]
        self.魔法防禦 = [0,0]
        self.爆擊倍率 = 1.5
        self.血量上限 = 30
        self.當前血量 = 30
        self.魔力上限 = 0
        self.當前魔力 = 0


        #
        self.骰子列表 = [野豬骰子()]
        self.掉落物列表 = [item.豬肉,equip.粗糙的豬皮手套]
        self.掉落機率 = [0.4,0.15] #PDF
        self.掉落金錢 = 15
        self.擊殺經驗 = 5
    async def 行動(self,我方列表,敵方列表):
        await super().行動(我方列表,敵方列表)
        #普通攻擊
        if(self.已行動 == False):
            攻擊目標 = sample(list(filter(lambda x:x.存活,敵方列表)),1)[0]
            文本 = await super().攻擊(攻擊目標)
            await messageReply.send(self.玩家.uid,文本)
            await super().行動結束()
        else:
            await super().行動結束()

class 刺蝟(生物):
    def __init__(self,名稱):
        super().__init__()
        self.名稱 = 名稱
        self.物理攻擊 = [4,0]
        self.物理防禦 = [10,0]
        self.速度 = [6,0]
        self.命中 = [12,0]
        self.爆擊 = [25,0]
        self.閃避 = [0,0]
        self.魔法攻擊 = [0,0]
        self.魔法防禦 = [0,0]
        self.爆擊倍率 = 2.0
        self.血量上限 = 60
        self.當前血量 = 60
        self.魔力上限 = 0
        self.當前魔力 = 0
        #
        self.骰子列表 = []
        self.掉落物列表 = [item.刺蝟的刺,item.刺蝟的刺,item.刺蝟的刺]
        self.掉落機率 = [0.2,0.2,0.2] #PDF
        self.掉落金錢 = 20
        self.擊殺經驗 = 10
        self.效果列表.append(尖刺(25))
    async def 行動(self,我方列表,敵方列表):
        await super().行動(我方列表,敵方列表)
        #普通攻擊
        if(self.已行動 == False):
            攻擊目標 = sample(list(filter(lambda x:x.存活,敵方列表)),1)[0]
            文本 = await super().攻擊(攻擊目標)
            await messageReply.send(self.玩家.uid,文本)
            await super().行動結束()
        else:
            await super().行動結束()

class 地鼠(生物):
    def __init__(self,名稱):
        super().__init__()
        self.名稱 = 名稱
        self.物理攻擊 = [8,0]
        self.物理防禦 = [0,0]
        self.速度 = [2,0]
        self.命中 = [12,0]
        self.爆擊 = [0,0]
        self.閃避 = [25,0]
        self.魔法攻擊 = [0,0]
        self.魔法防禦 = [0,0]
        self.爆擊倍率 = 1.5
        self.血量上限 = 30
        self.當前血量 = 30
        self.魔力上限 = 0
        self.當前魔力 = 0
        self.地屬性攻擊力 = [8,0]
        #
        self.骰子列表 = []
        self.掉落物列表 = []
        self.掉落機率 = [] #PDF
        self.掉落金錢 = 12
        self.擊殺經驗 = 5
    async def 行動(self,我方列表,敵方列表):
        await super().行動(我方列表,敵方列表)
        #普通攻擊
        if(self.已行動 == False):
            目標 = sample(list(filter(lambda x:x.存活,敵方列表)),1)[0]
            文本 = ""
            (傷害,爆擊,傷害文本) = await 新傷害公式(self,目標,[["地屬性",0.5],["物理",0.5],['命中',1.0],['爆擊',1.0]],['近戰','攻擊'])
            if(傷害!=-1):
                if(爆擊):
                    文本 += self.名稱+" 對 "+目標.名稱+" 發動攻擊，造成了 "+str(傷害) +" 點傷害。爆擊\n"
                else:
                    文本 += self.名稱+" 對 "+目標.名稱+" 發動攻擊，造成了 "+str(傷害) +" 點傷害。\n"
            else:
                文本 += self.名稱+" 對 "+目標.名稱+" 發動攻擊，但沒有命中\n"
                文本 += 傷害文本
            await messageReply.send(self.玩家.uid,文本)
            await super().行動結束()
        else:
            await super().行動結束()

class 地鼠王(生物):
    def __init__(self,名稱):
        super().__init__()
        self.名稱 = 名稱
        self.物理攻擊 = [15,0]
        self.物理防禦 = [0,0]
        self.速度 = [2,0]
        self.命中 = [18,0]
        self.爆擊 = [0,0]
        self.閃避 = [30,0]
        self.魔法攻擊 = [0,0]
        self.魔法防禦 = [0,0]
        self.爆擊倍率 = 1.5
        self.血量上限 = 150
        self.當前血量 = 150
        self.魔力上限 = 0
        self.當前魔力 = 0
        self.地屬性攻擊力 = [12,0]
        #
        self.骰子列表 = []
        self.掉落物列表 = [equip.地鼠王盔甲]
        self.掉落機率 = [0.75] #PDF
        self.掉落金錢 = 100
        self.擊殺經驗 = 25
        #
        self.挖洞=False
    async def 行動(self,我方列表,敵方列表):
        await super().行動(我方列表,敵方列表)
        #普通攻擊
        if(self.已行動 == False):
            目標 = sample(list(filter(lambda x:x.存活,敵方列表)),1)[0]
            文本 = ""
            (傷害,爆擊,傷害文本) = await 新傷害公式(self,目標,[["地屬性",0.5],["物理",0.5],['命中',1.0],['爆擊',1.0]],['近戰','攻擊'])
            if(傷害!=-1):
                if(爆擊):
                    文本 += self.名稱+" 對 "+目標.名稱+" 發動攻擊，造成了 "+str(傷害) +" 點傷害。爆擊\n"
                else:
                    文本 += self.名稱+" 對 "+目標.名稱+" 發動攻擊，造成了 "+str(傷害) +" 點傷害。\n"
            else:
                文本 += self.名稱+" 對 "+目標.名稱+" 發動攻擊，但沒有命中\n"
                文本 += 傷害文本
            await messageReply.send(self.玩家.uid,文本)
            await super().行動結束()
        else:
            await super().行動結束()    
    async def 死亡(self):
        self.存活 = False
        print(self.名稱+"死亡")
        文本 = self.名稱+"死亡\n"
        #await messageReply.send(self.玩家.uid,文本)
        for 目標 in self.戰場.怪物方:
            目標.存活=False
        文本 += await self.戰場.戰場死亡處理(self)
        return 文本
class 地洞(生物):
    def __init__(self,名稱,標記):
        super().__init__()
        self.名稱 = 名稱
        self.血量上限 = 5
        self.當前血量 = 5
        self.速度 = [0,0]
        self.標記 = 標記
        self.怪物_地鼠王 = None
        #
        self.骰子列表 = []
        self.效果列表.append(融化())

    async def 行動(self,我方列表,敵方列表):
        await self.行動結束()
    async def 行動結束(self):
        await super().行動結束()
    async def 死亡(self):
        self.存活 = False
        print(self.名稱+"死亡")
        文本 = self.名稱+"被破壞了\n"
     
        if(self.標記=="炸藥"):
            文本+="不好，地洞裡藏有的炸藥被引爆了。\n"
            for 目標 in self.戰場.玩家方:
                (傷害,爆擊,傷害文本)= await 新傷害公式(self,目標,[["火傷",10]],[])
                文本+=目標.名稱 + " 被炸藥炸傷，受到了 " + str(傷害) + " 點傷害\n"
                文本+=傷害文本
        if(self.標記=="地鼠"):
            文本+="一隻地鼠從破損的地洞中鑽了出來。\n"
            怪物_地鼠=地鼠("地鼠")
            怪物_地鼠.戰場 = self.戰場
            怪物_地鼠.玩家 = self.玩家
            self.戰場.怪物方.append(怪物_地鼠)
        if(self.標記=="地鼠王"):
            文本+="地鼠王從破損的地洞中鑽了出來。\n"
            self.戰場.怪物方.append(self.戰場.怪物_地鼠王)
        文本 += await self.戰場.戰場死亡處理(self)
        return 文本    
class 飛鳥(生物):
    def __init__(self,名稱,型態):
        super(飛鳥, self).__init__()
        self.名稱 = 名稱
        self.物理攻擊 = [6,0]
        self.物理防禦 = [8,0]
        self.速度 = [6,0]
        self.命中 = [10,0]
        self.爆擊 = [0,0]
        self.閃避 = [4,0]
        self.魔法攻擊 = [0,0]
        self.魔法防禦 = [0,0]
        self.爆擊倍率 = 1.5
        self.血量上限 = 50
        self.當前血量 = 50
        self.魔力上限 = 0
        self.當前魔力 = 0

        #
        self.骰子列表 = [飛鳥骰子(型態)]
        self.掉落物列表 = [equip.粗糙的鳥羽毛帽]
        self.掉落機率 = [0.2] #PDF
        self.掉落金錢 = 15
        self.擊殺經驗 = 8
    async def 行動(self,我方列表,敵方列表):
        await super().行動(我方列表,敵方列表)
        #普通攻擊
        if(self.已行動 == False):
            攻擊目標 = sample(list(filter(lambda x:x.存活,敵方列表)),1)[0]
            文本 = await super().攻擊(攻擊目標)
            await messageReply.send(self.玩家.uid,文本)
            await super().行動結束()
        else:
            await super().行動結束()        

#召喚雪人 召出來的Token
class 雪人(生物):
    def __init__(self,名稱):
        super().__init__()
        self.名稱 = 名稱
        self.血量上限 = 20
        self.當前血量 = 20
        self.速度 = [0,0]
        self.倒數 = 4
        #
        self.骰子列表 = []
        self.效果列表.append(融化())

    async def 行動(self,我方列表,敵方列表):
        await self.行動結束()
    async def 行動結束(self):
        
        融化=self.含有效果("快要融化了")
        if(融化==False):
            await self.死亡()
            文本 = self.名稱+"融化了0.0"
            await messageReply.send(self.玩家.uid,文本)
            #await self.戰場.戰場死亡處理(self)
        await super().行動結束()
class 魔法雪人(生物):
    def __init__(self,名稱):
        super().__init__()
        self.名稱 = 名稱
        self.物理攻擊 = [0,0]
        self.物理防禦 = [20,0]
        self.速度 = [0,0]
        self.命中 = [0,0]
        self.爆擊 = [0,0]
        self.閃避 = [0,0]
        self.魔法攻擊 = [0,0]
        self.魔法防禦 = [20,0]
        self.爆擊倍率 = 1.5
        self.血量上限 = 60
        self.當前血量 = 60
        self.魔力上限 = 0
        self.當前魔力 = 0
        self.雷屬性防禦力 = [20,0]
        self.火屬性防禦力 = [20,0]
        self.冰屬性防禦力 = [20,0]
        self.地屬性防禦力 = [20,0]
        self.光屬性防禦力 = [20,0]
        self.暗屬性防禦力 = [20,0]
        #
        self.骰子列表 = []
        self.掉落物列表 = []
        self.掉落機率 = [] #PDF
        self.掉落金錢 = 0
        self.擊殺經驗 = 10
        self.技能列表 = [skill.霜寒吐息()]
        #
    async def 行動(self,我方列表,敵方列表):
        await super().行動(我方列表,敵方列表)

        if(self.已行動 == False):
            self.當前使用技能= self.技能列表[0]
            目標 = sample(list(filter(lambda x:x.存活,敵方列表)),1)[0]
            await self.使用技能(目標)
        await super().行動結束()  

class 蝙蝠(生物):
    def __init__(self,名稱):
        super(蝙蝠, self).__init__()
        self.名稱 = 名稱
        self.物理攻擊 = [4,0]
        self.物理防禦 = [8,0]
        self.速度 = [4,0]
        self.命中 = [15,0]
        self.爆擊 = [5,0]
        self.閃避 = [6,0]
        self.魔法攻擊 = [0,0]
        self.魔法防禦 = [0,0]
        self.爆擊倍率 = 1.5
        self.血量上限 = 28
        self.當前血量 = 28
        self.魔力上限 = 0
        self.當前魔力 = 0
        #
        self.骰子列表 = [蝙蝠骰子()]
        self.掉落物列表 = []
        self.掉落機率 = [] #PDF
        self.掉落金錢 = 10
        self.擊殺經驗 = 6
        #

    async def 行動(self,我方列表,敵方列表):
        await super().行動(我方列表,敵方列表)
        #普通攻擊
        
        if(self.已行動 == False):
            if(self.行為=="行動:倒吊"):
                文本 = self.名稱+"倒吊在空中休息，回復了8點生命"
                await messageReply.send(self.玩家.uid,文本)
                self.回復生命(8,self)
            if(self.行為=="行動:超音波"):
                目標 = sample(list(filter(lambda x:x.存活,敵方列表)),1)[0]
                文本 = self.名稱+" 對 "+目標.名稱 +" 使用了超音波，吸取3點閃避，持續2回合。"
                await messageReply.send(self.玩家.uid,文本)
                閃避Buff  =  閃避增加效果(3,2)
                閃避DeBuff  =  閃避增加效果(-3,2)
                self.效果列表.append(閃避Buff)
                閃避Buff.獲得效果(self)
                目標.效果列表.append(閃避DeBuff)
                閃避DeBuff.獲得效果(目標)
            if(self.行為=="行動:盤旋"):
                目標 = sample(list(filter(lambda x:x.存活,敵方列表)),1)[0]
                文本 = self.名稱+" 對 "+目標.名稱 +" 使用了盤旋，吸取3點爆擊，持續2回合。"
                await messageReply.send(self.玩家.uid,文本)
                爆擊Buff  =  爆擊增加效果(3,2)
                爆擊DeBuff  =  爆擊增加效果(-3,2)
                self.效果列表.append(爆擊Buff)
                爆擊Buff.獲得效果(self)
                目標.效果列表.append(爆擊DeBuff)
                爆擊DeBuff.獲得效果(目標)
            if(self.行為=="行動:俯衝攻擊"):  
                攻擊目標 = sample(list(filter(lambda x:x.存活,敵方列表)),1)[0]
                文本 = await super().攻擊(攻擊目標)
                await messageReply.send(self.玩家.uid,文本)
            await super().行動結束()
        else:
            await super().行動結束()
        

class 企鵝(生物):
    def __init__(self,名稱):
        super(企鵝, self).__init__()
        self.名稱 = 名稱
        self.物理攻擊 = [6,0]
        self.物理防禦 = [8,0]
        self.速度 = [6,0]
        self.命中 = [20,0]
        self.爆擊 = [10,0]
        self.閃避 = [10,0]
        self.魔法攻擊 = [16,0]
        self.魔法防禦 = [0,0]
        self.爆擊倍率 = 1.5
        self.血量上限 = 45
        self.當前血量 = 45
        self.魔力上限 = 0
        self.當前魔力 = 0
        #
        self.骰子列表 = [企鵝骰子()]
        self.掉落物列表 = []
        self.掉落機率 = [] #PDF
        self.掉落金錢 = 20
        self.擊殺經驗 = 12
        self.技能列表 = [skill.冰箭()]
        #

    async def 行動(self,我方列表,敵方列表):
        await super().行動(我方列表,敵方列表)
        #普通攻擊
        if(self.已行動 == False):
            if(self.行為=="行動:冰箭"):
                self.當前使用技能= self.技能列表[0]
                目標 = sample(list(filter(lambda x:x.存活,敵方列表)),1)[0]
                await self.使用技能(目標)
            if(self.行為=="行動:撞擊"):  
                攻擊目標 = sample(list(filter(lambda x:x.存活,敵方列表)),1)[0]
                文本 = await super().攻擊(攻擊目標)
                await messageReply.send(self.玩家.uid,文本)
        await super().行動結束()  

class 雪原狼(生物):
    def __init__(self,名稱):
        super(雪原狼, self).__init__()
        self.名稱 = 名稱
        self.物理攻擊 = [5,0]
        self.物理防禦 = [8,0]
        self.速度 = [7,0]
        self.命中 = [20,0]
        self.爆擊 = [15,0]
        self.閃避 = [10,0]
        self.魔法攻擊 = [0,0]
        self.魔法防禦 = [0,0]
        self.爆擊倍率 = 1.5
        self.血量上限 = 45
        self.當前血量 = 45
        self.魔力上限 = 0
        self.當前魔力 = 0
        #
        self.骰子列表 = [雪原狼骰子()]
        self.掉落物列表 = [item.雪原狼的爪子,item.雪原狼的爪子,equip.粗糙的狼毫毛靴]
        self.掉落機率 = [0.25,0.25,0.15] #PDF
        self.掉落金錢 = 20
        self.擊殺經驗 = 12
        self.技能列表 = [skill.怒吼()]
        #

    async def 行動(self,我方列表,敵方列表):
        await super().行動(我方列表,敵方列表)
        #普通攻擊
        if(self.已行動 == False):
            if(self.行為=="行動:怒吼"):  
                self.當前使用技能= self.技能列表[0]
                await self.使用技能(self)
            else:
                攻擊目標 = sample(list(filter(lambda x:x.存活,敵方列表)),1)[0]
                文本 = await super().攻擊(攻擊目標)
                await messageReply.send(self.玩家.uid,文本)
        await super().行動結束()
            
class 效果:
    def __init__(self):
        self.名稱 = ""
        self.等級 = 0
        self.持續回合數 = 0
        self.經過回合數 = 0
        self.衰退 = True
    def 獲得效果(self,效果持有者):
        return "this is abstract function"
    def 回合開始(self,效果持有者):
        return "this is abstract function"
    def 回合結束(self,效果持有者):
        return "this is abstract function"
    def 失去效果(self,效果持有者):
        return "this is abstract function"
    

class 攻擊增加效果(效果):
    def __init__(self,等級,持續回合數):
        super().__init__()
        self.名稱 = "攻擊增加"
        self.等級 = 等級
        self.持續回合數 = 持續回合數
    def 獲得效果(self,效果持有者):
        效果持有者.物理攻擊[1] += self.等級
    def 失去效果(self,效果持有者):
        效果持有者.物理攻擊[1] -= self.等級

class 憤怒(效果):
    def __init__(self,等級,持續回合數):
        super().__init__()
        self.名稱 = "憤怒"
        self.等級 = 等級
        self.持續回合數 = 持續回合數
    def 獲得效果(self,效果持有者):
        效果持有者.物理攻擊[1] += self.等級
    def 失去效果(self,效果持有者):
        效果持有者.物理攻擊[1] -= self.等級
        
class 防禦增加效果(效果):
    def __init__(self,等級,持續回合數):
        super().__init__()
        self.名稱 = "防禦增加"
        self.等級 = 等級
        self.持續回合數 = 持續回合數
    def 獲得效果(self,效果持有者):
        效果持有者.物理防禦[1] += self.等級
    def 失去效果(self,效果持有者):
        效果持有者.物理防禦[1] -= self.等級


class 速度增加效果(效果):
    def __init__(self,等級,持續回合數):
        super().__init__()
        self.名稱 = "速度增加"
        self.等級 = 等級
        self.持續回合數 = 持續回合數
    def 獲得效果(self,效果持有者):
        效果持有者.速度[1] += self.等級
    def 失去效果(self,效果持有者):
        效果持有者.速度[1] -= self.等級
        
class 閃避增加效果(效果):
    def __init__(self,等級,持續回合數):
        super().__init__()
        self.名稱 = "閃避增加"
        self.等級 = 等級
        self.持續回合數 = 持續回合數
    def 獲得效果(self,效果持有者):
        效果持有者.閃避[1] += self.等級
    def 失去效果(self,效果持有者):
        效果持有者.閃避[1] -= self.等級

               
class 冰緩(效果):
    def __init__(self,等級,持續回合數):
        super().__init__()
        self.名稱 = "冰緩"
        self.等級 = 等級
        self.持續回合數 = 持續回合數
    def 獲得效果(self,效果持有者):
        效果持有者.速度[1] -= self.等級
    def 失去效果(self,效果持有者):
        效果持有者.速度[1] += self.等級
        
class 爆擊增加效果(效果):
    def __init__(self,等級,持續回合數):
        super().__init__()
        self.名稱 = "爆擊增加"
        self.等級 = 等級
        self.持續回合數 = 持續回合數
    def 獲得效果(self,效果持有者):
        效果持有者.爆擊[1] += self.等級
    def 失去效果(self,效果持有者):
        效果持有者.爆擊[1] -= self.等級

class 火屬性攻擊力增加效果(效果):
    def __init__(self,等級,持續回合數):
        super().__init__()
        self.名稱 = "火屬性攻擊力增加"
        self.等級 = 等級
        self.持續回合數 = 持續回合數
    def 獲得效果(self,效果持有者):
        效果持有者.火屬性攻擊力[1] += self.等級
    def 失去效果(self,效果持有者):
        效果持有者.火屬性攻擊力[1] -= self.等級

class 雷屬性攻擊力增加效果(效果):
    def __init__(self,等級,持續回合數):
        super().__init__()
        self.名稱 = "雷屬性攻擊力增加"
        self.等級 = 等級
        self.持續回合數 = 持續回合數
    def 獲得效果(self,效果持有者):
        效果持有者.雷屬性攻擊力[1] += self.等級
    def 失去效果(self,效果持有者):
        效果持有者.雷屬性攻擊力[1] -= self.等級
        
class 暈眩(效果):
    def __init__(self,持續回合數):
        super().__init__()
        self.名稱 = "暈眩"
        self.等級 = 0
        self.衰退 = False
        self.持續回合數 = 持續回合數

class 燃燒(效果):
    def __init__(self,等級,持續回合數):
        super().__init__()
        self.名稱 = "燃燒"
        self.等級 = 等級
        self.持續回合數 = 持續回合數

class 流血(效果):
    def __init__(self,等級,持續回合數):
        super().__init__()
        self.名稱 = "流血"
        self.等級 = 等級
        self.持續回合數 = 持續回合數
        
class 麻痺(效果):
    def __init__(self,等級,持續回合數):
        super().__init__()
        self.名稱 = "麻痺"
        self.等級 = 等級
        self.持續回合數 = 持續回合數

class 石化(效果):
    def __init__(self,等級,持續回合數):
        super().__init__()
        self.名稱 = "石化"
        self.等級 = 等級
        self.持續回合數 = 持續回合數
    def 獲得效果(self,效果持有者):
        效果持有者.速度[1] -= self.等級
        效果持有者.物理防禦[1] += self.等級*3
    def 失去效果(self,效果持有者):
        效果持有者.速度[1] += self.等級
        效果持有者.物理防禦[1] -= self.等級*3
        
class 鬼火(效果):
    def __init__(self):
        super().__init__()
        self.名稱 = "鬼火"
        self.等級 = 1
        self.衰退 = False
        self.持續回合數 = 1
    def 獲得效果(self,效果持有者):
        效果持有者.火屬性攻擊力[1] += 8
    def 失去效果(self,效果持有者):
        效果持有者.火屬性攻擊力[1] -= self.等級*8

class 尖刺(效果):
    def __init__(self,等級):
        super().__init__()
        self.名稱 = "尖刺"
        self.等級 = 等級
        self.衰退 = False
        self.持續回合數 = 1

class 融化(效果):
    def __init__(self):
        super().__init__()
        self.名稱 = "快要融化了"
        self.持續回合數 = 3       
        
class 料理_命中增加效果(效果):
    def __init__(self,等級,持續回合數):
        super().__init__()
        self.名稱 = "料理_命中增加"
        self.等級 = 等級
        self.持續回合數 = 持續回合數
    def 獲得效果(self,效果持有者):
        效果持有者.命中[1] += self.等級
    def 失去效果(self,效果持有者):
        效果持有者.命中[1] -= self.等級

class 料理_閃避增加效果(效果):
    def __init__(self,等級,持續回合數):
        super().__init__()
        self.名稱 = "料理_閃避增加"
        self.等級 = 等級
        self.持續回合數 = 持續回合數
    def 獲得效果(self,效果持有者):
        效果持有者.閃避[1] += self.等級
    def 失去效果(self,效果持有者):
        效果持有者.閃避[1] -= self.等級

class 料理_爆擊增加效果(效果):
    def __init__(self,等級,持續回合數):
        super().__init__()
        self.名稱 = "料理_爆擊增加"
        self.等級 = 等級
        self.持續回合數 = 持續回合數
    def 獲得效果(self,效果持有者):
        效果持有者.爆擊[1] += self.等級
    def 失去效果(self,效果持有者):
        效果持有者.爆擊[1] -= self.等級
        
class 骰子:
    def __init__(self):
        self.當前點數 = 0
        self.面數 = 0
        self.內容 = list()
        self.回合 = -1
    def 擲骰(self):
        self.當前點數 = random.randint(0, self.面數-1)
        return self.當前點數
        
class 新手骰子_攻擊(骰子):
    def __init__(self):
        super().__init__()
        self.面數 = 6
        self.內容 = ["攻擊+3","攻擊+5","空","空","空","空"]
        self.效果 = [攻擊增加效果(3,1),攻擊增加效果(5,1),None,None,None,None]

class 新手骰子_防禦(骰子):
    def __init__(self):
        super().__init__()
        self.面數 = 6
        self.內容 = ["防禦+4","防禦+8","空","空","空","空"]
        self.效果 = [防禦增加效果(4,1),防禦增加效果(8,1),None,None,None,None]

class 初級雷骰(骰子):
    def __init__(self):
        super().__init__()
        self.面數 = 5
        self.內容 = ["雷屬性攻擊力+2","雷屬性攻擊力+4","雷屬性攻擊力+6","雷屬性攻擊力+8","雷屬性攻擊力+10"]
        self.效果 = [雷屬性攻擊力增加效果(2,1),雷屬性攻擊力增加效果(4,1),雷屬性攻擊力增加效果(6,1),雷屬性攻擊力增加效果(8,1),雷屬性攻擊力增加效果(10,1)]

class 落石骰子(骰子):
    def __init__(self):
        super().__init__()
        self.面數 = 5
        self.傷害 = 0
        self.內容 = ["落石","落石","落石","空","空"]
        self.效果 = [None,None,None,None,None]
        
class 野豬骰子(骰子):
    def __init__(self):
        super().__init__()
        self.面數 = 6
        self.內容 = ["速度+2","攻擊+4","防禦+4","空","空","空"]
        self.效果 = [速度增加效果(2,1),攻擊增加效果(4,1),防禦增加效果(4,1),None,None,None]
        
class 飛鳥骰子(骰子):
    def __init__(self,型態):
        super().__init__()
        self.面數 = 5
        self.陸內容 = ["攻擊+2","攻擊+4","攻擊+6","飛行","空"]
        self.陸效果 = [攻擊增加效果(2,1),攻擊增加效果(4,1),攻擊增加效果(6,1),None,None]
        self.空內容 = ["閃避+4","閃避+6","閃避+8","降落","空"]
        self.空效果 = [閃避增加效果(4,1),閃避增加效果(6,1),閃避增加效果(8,1),None,None]
        if(型態=="陸"):
            self.內容 = self.陸內容
            self.效果 = self.陸效果
        else:
            self.內容 = self.空內容
            self.效果 = self.空效果

class 蝙蝠骰子(骰子):
    def __init__(self):
        super(  ).__init__()
        self.面數 = 6
        self.內容 = ["行動:倒吊","行動:超音波","行動:盤旋","行動:俯衝攻擊","行動:俯衝攻擊","行動:俯衝攻擊"]
        self.效果 = [None,None,None,None,None,None]

class 企鵝骰子(骰子):
    def __init__(self):
        super(  ).__init__()
        self.面數 = 3
        self.內容 = ["行動:冰箭","行動:撞擊","行動:撞擊"]
        self.效果 = [None,None,None]

class 雪原狼骰子(骰子):
    def __init__(self):
        super(  ).__init__()
        self.面數 = 5
        self.內容 = ["迅捷","迅捷","行動:怒吼","空","空"]
        self.效果 = [None,None,None,None,None]
        
class 戰鬥():
    def __init__(self):
        self.玩家方 = list()
        self.所有玩家方 = list()
        self.怪物方 = list()
        self.當前行動者 = None
        self.戰鬥結束 = False
        self.玩家 = False
        self.狀態 = None
        self.戰利品 = list()
        self.金錢獎勵 = 0
        self.經驗獎勵 = 0
        self.勝利事件 = None
    async def 取得下一個行動目標(self):
        順序 = self.玩家方 + self.怪物方
        random.shuffle (順序)
        list.sort(順序, key=lambda x: x.速度[0]+x.速度[1], reverse=True)
        順序 = list(filter(lambda x:not x.已行動 and x.存活,順序))
        if(len(順序)==0 or self.戰鬥結束 == True):
            await self.回合結束()
            return None
        else:
            return 順序[0]
    async def 擲骰階段(self):
        文本 ="-----擲骰階段-----\n"
        #await messageReply.send(self.玩家.uid,文本)
        
        順序 = self.玩家方 + self.怪物方
        順序 = list(filter(lambda x:not x.已行動 and x.存活,順序))
        random.shuffle (順序)
        list.sort(順序, key=lambda x: x.速度[0]+x.速度[1], reverse=True)
        for a in 順序:
            文本+=await a.生物擲骰()
        print("---行動階段---")
        文本 += self.取得戰場資訊()
        await messageReply.send(self.玩家.uid,文本)
    async def 戰鬥階段(self):
        if(self.戰鬥結束):
            return
        行動者 = await self.取得下一個行動目標()
        if(行動者 != None):
            self.當前行動者 = 行動者
            print("當前行動者"+self.當前行動者.名稱)
            if(self.當前行動者 in self.玩家方):
                await self.當前行動者.行動(self.玩家方,self.怪物方)
            else:
                await self.當前行動者.行動(self.怪物方,self.玩家方)
    async def 回合開始(self):
        if(self.戰鬥結束==False):
            文本 = "================================\n"
            文本 +="-----回合開始-----"
            await messageReply.send(self.玩家.uid,文本)
        #print("======================================================================")
        #print("---回合開始---")
            順序 = self.玩家方 + self.怪物方
            for a in 順序:
                a.已行動=False
            await self.擲骰階段()
            await self.戰鬥階段()
    async def 使用技能(self,message):
        self.狀態 = None
        文本 = "你選擇了使用 "+ self.當前行動者.當前使用技能.名稱+"\n"
        if(self.當前行動者.當前使用技能.類型=="敵方單體"):
            self.狀態 = "選擇技能目標"
            文本 += "選擇技能施放目標\n"
            for a in range(len(self.怪物方)):
                文本+=str(a)+":"+self.怪物方[a].名稱+"\n"
            文本+="c:取消\n"
            await messageReply.send(self.玩家.uid,文本)
        if(self.當前行動者.當前使用技能.類型=="單體"):
            self.狀態 = "選擇技能目標"
            文本 = "選擇技能施放目標\n"
            for a in range(len(self.玩家方)):
                文本+=str(a)+":"+self.玩家方[a].名稱+"\n"
            for a in range(len(self.怪物方)):
                文本+=str(len(self.玩家方)+a)+":"+self.怪物方[a].名稱+"\n"
            文本+="c:取消\n"
            await messageReply.send(self.玩家.uid,文本)
        if(self.當前行動者.當前使用技能.類型=="敵方群體"):
            self.狀態 = None
            await messageReply.send(self.玩家.uid,文本)
            await self.當前行動者.使用技能(self.怪物方)
            await self.當前行動者.行動結束()
        if(self.當前行動者.當前使用技能.名稱=="召喚雪人"):
            self.狀態 = None
            await messageReply.send(self.玩家.uid,文本)
            await self.當前行動者.使用技能(self.玩家方)
            await self.當前行動者.行動結束()
        if(self.當前行動者.當前使用技能.類型=="立即施放"):
            self.狀態 = None
            await messageReply.send(self.玩家.uid,文本)
            await self.當前行動者.使用技能(self.玩家方)
            await self.當前行動者.行動結束()
    async def 處理訊息(self,message):
        msg = message.content.lower()
        user = message.author
        username = user.name
        userid = user.id
        if(self.狀態==None):
            if(msg=="a"):
                文本 = "請選擇攻擊目標\n"
                for a in range(len(self.怪物方)):
                    文本+=str(a)+":"+self.怪物方[a].名稱+"\n"
                文本+="c:取消\n"
                await messageReply.send(self.玩家.uid,文本)
                self.狀態 ="攻擊選擇目標"
            if(msg=="s"):
                await self.當前行動者.取得技能資訊()
                self.狀態 ="選擇使用技能"
            if(msg=="b"):
                await self.當前行動者.取得戰鬥物品資訊()
                self.狀態 ="選擇使用物品"
            if(msg=="i"):
                await self.取得詳細戰場資訊()
            if(msg=="r" and self.當前行動者.可以逃跑):
                self.狀態 = "逃跑"
                文本 = "逃跑成功，輸入任意值確認"
                await self.戰鬥結束處理()
                await messageReply.send(self.玩家.uid,文本)
        elif(self.狀態=="攻擊選擇目標"):
            if (msg=="c"):
                self.狀態 = None
                await self.當前行動者.行動(self.玩家方,self.怪物方)
            else:
                x = int(msg)
                if(x>=0 and x<len(self.怪物方)):
                    print(x)
                    self.狀態 = None
                    文本 = await self.當前行動者.攻擊(self.怪物方[x])
                    await messageReply.send(self.玩家.uid,文本)
                    await self.當前行動者.行動結束()
        elif(self.狀態=="選擇使用技能"):
            if (msg=="c"):
                self.狀態 = None
                await self.當前行動者.行動(self.玩家方,self.怪物方)
            else:
                x = int(msg)
                if(x>=0 and x<len(self.當前行動者.技能列表)):
                    print(x)
                   
                    self.當前行動者.當前使用技能=self.當前行動者.技能列表[x]
                    if(self.當前行動者.當前使用技能!=None):
                        if (self.當前行動者.技能列表[x].當前冷卻 != 0):
                            文本 = "該技能正在冷卻中"
                            await messageReply.send(self.玩家.uid,文本)
                        elif(self.當前行動者.技能列表[x].消耗魔力>self.當前行動者.當前魔力+self.當前行動者.水):
                            文本 = "魔力不足"
                            await messageReply.send(self.玩家.uid,文本)
                        else:
                            耗魔 = self.當前行動者.技能列表[x].消耗魔力
                            #扣除魔力
                            if(self.當前行動者.水>=耗魔):
                                self.當前行動者.水-=耗魔
                                self.當前行動者.耗水強化 = True
                            else:
                                self.當前行動者.耗水強化 = False
                                self.當前行動者.當前魔力-=(耗魔-self.當前行動者.水)
                                self.當前行動者.水 = 0
                            await self.使用技能(message)                            
                            
        elif(self.狀態=="選擇技能目標"):
            if (msg=="c"):
                self.狀態 = "選擇使用技能"
                await self.當前行動者.取得技能資訊()
            elif(self.當前行動者.當前使用技能!=None):
                if(self.當前行動者.當前使用技能.類型=="敵方單體"):   
                    x = int(msg)
                    if(x>=0 and x<len(self.怪物方)):
                        print(x)
                        self.狀態 = None
                        await self.當前行動者.使用技能(self.怪物方[x])
                        await self.當前行動者.行動結束()
                elif(self.當前行動者.當前使用技能.類型=="單體"):   
                    x = int(msg)
                    if(x>=0 and x<len(self.玩家方)):
                        print(x)
                        self.狀態 = None
                        await self.當前行動者.使用技能(self.玩家方[x])
                        await self.當前行動者.行動結束()
                    elif(x>len(self.玩家方) and x<len(self.玩家方)+len(self.怪物方)):
                        print(x)
                        self.狀態 = None
                        await self.當前行動者.使用技能(self.怪物方[x-len(self.玩家方)])
                        await self.當前行動者.行動結束()
        elif(self.狀態=="選擇使用物品"):
            if(msg=="c"):
                self.狀態 = None
                await self.當前行動者.行動(self.玩家方,self.怪物方)
            else:
                x = int(msg)
                if(x>=0 and x<len(self.當前行動者.玩家.背包.戰鬥物品對應)):
                    self.當前行動者.當前使用技能=self.當前行動者.玩家.背包.戰鬥物品對應[x].技能
                    await self.使用技能(message) 
                    print(x)
                        
            
    async def 回合結束(self):
        print("---回合結束---")
        順序 = self.玩家方 + self.怪物方
        list.sort(順序, key=lambda x: x.速度[0]+x.速度[1], reverse=True)
        順序 = list(filter(lambda x:x.存活,順序))
        for a in 順序:
            await a.回合結束()
        self.玩家方 = list(filter(lambda x:x.存活,self.玩家方))
        self.怪物方 = list(filter(lambda x:x.存活,self.怪物方))
        #self.當前行動者.可以逃跑 = False;
        await self.回合開始()

    async def 戰場死亡處理(self,目標):
        文本 = ""
        if(目標.是玩家):
            #self.玩家方.remove(目標)
            無="無"
        else:
        #    self.怪物方.remove(目標)
            for a in range(len(目標.掉落物列表)):
                隨機數 = random.random()
                if(隨機數<目標.掉落機率[a]):
                    self.戰利品.append(目標.掉落物列表[a])
            self.金錢獎勵+=目標.掉落金錢
            self.經驗獎勵+=目標.擊殺經驗
        玩家存活方 = list(filter(lambda x:x.存活,self.玩家方))
        if(len(玩家存活方)==0):
            self.狀態 = "怪物方勝利"
            await self.戰鬥結束處理()
            print("怪物方勝利")
            文本 = "怪物方勝利\n"
            文本 += "輸入任意值進行確定"
            self.戰鬥結束 = True
            #await messageReply.send(self.玩家.uid,文本)
           
        怪物存活方 = list(filter(lambda x:x.存活,self.怪物方))
        if(len(怪物存活方)==0):
            self.戰鬥結束 = True
            await self.戰鬥結束處理()
            print("玩家方勝利")       
            文本 ="玩家方勝利\n"
            文本 += "輸入任意值進行確定"
            self.狀態 = "玩家方勝利"
            
            #await messageReply.send(self.玩家.uid,文本)
        return 文本 
    async def 戰鬥結束處理(self):
        for a in self.所有玩家方:
            a.水 = 0
            for 技能 in a.技能列表:
                技能.當前冷卻=0
            for 效果 in a.效果列表:
                效果.失去效果(a)
            a.效果列表 = list()
                
    def 取得戰場資訊(self):
        文本 = ""
        #print("---戰場資訊---")
        #print("---玩家方---")
        文本+="玩家方\n"
        for a in self.玩家方:
            文本+=a.取得資訊()
            #print(a.取得資訊())
            #embed = a.取得InfoEmbed()
            #await messageReply.sendembed(self.玩家.uid,"",embed)
        #print("---怪物方---")
        文本+="怪物方\n"
        for a in self.怪物方:
            文本+=a.取得資訊()
            #print(a.取得資訊())
            #embed = a.取得InfoEmbed()
            #await messageReply.sendembed(self.玩家.uid,"",embed)
        #await messageReply.send(self.玩家.uid,文本)
        #print("-----------")
        return 文本
    async def 取得詳細戰場資訊(self):
        文本 = "====戰場資訊====\n"
        文本 += "===玩家方===\n"
        #await messageReply.send(self.玩家.uid,"---戰場資訊---")
        #print("---戰場資訊---")
        #print("---玩家方---")
        #await messageReply.send(self.玩家.uid,"---玩家方---")
        #await messageReply.send(self.玩家.uid,文本)
        for a in self.玩家方:
            #print(a.取得資訊())
            文本 += "------------------------\n"
            文本 += a.取得詳細InfoEmbed()
            #await messageReply.sendembed(self.玩家.uid,"",embed)
            #await messageReply.send(self.玩家.uid,文本)
        #print("---怪物方---")
        文本 += "===怪物方===\n"
        #await messageReply.send(self.玩家.uid,"---怪物方---")
        for a in self.怪物方:
            #print(a.取得資訊())
            文本 += "------------------------\n"
            文本 += a.取得詳細InfoEmbed()
            #await messageReply.sendembed(self.玩家.uid,"",embed)
        await messageReply.send(self.玩家.uid,文本)
        #print("-----------")


       
'''
玩家人物 = 玩家角色()
野豬A = 野豬("野豬A")
野豬B = 野豬("野豬B")

F = 戰鬥()
F.玩家方.append(玩家人物)
F.怪物方.append(野豬A)
F.怪物方.append(野豬B)
玩家人物.戰場 = F
野豬A.戰場 = F
野豬B.戰場 = F

文本="你遇到了兩隻野豬"

print(文本)

F.回合開始()
F.戰鬥階段()
#print(F.取得下一個行動目標())
'''

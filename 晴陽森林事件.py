import event
import messageReply
import item
import equip
import random
import NEWRPG
import globals
import 公共事件

#晴陽森林事件
class 晴陽森林_找到溪流(event.事件):
    def __init__(self):
        self.名稱 = "晴陽森林_找到溪流"
    async def 執行事件(self):
        玩家 = self.玩家
        文本 = "=====================================" + "\n"
        文本 += "你從遠處聽到了流水的聲音。" + "\n"
        文本 += "你朝聲音走去，找到了一條潺潺的溪流。" + "\n"
        文本 += "你想要做甚麼?" + "\n"
        文本 += "=====================================" + "\n"
        文本 += "1:釣魚" + "\n"
        文本 += "2:裝水" + "\n"
        文本 += "3:泛舟" + "\n"
        文本 += "=====================================" + "\n"
        await messageReply.send(玩家.uid,文本)
    async def 處理事件(self,message):
        msg =message.content.lower()
        玩家 = self.玩家
        if(玩家.鎖):
            msg = None
        if(msg=="1"):
            玩家.鎖 = True
            文本 = "你決定釣魚，但你沒有釣竿，所以你只好跳進溪裡雙手一撈。"
            await messageReply.send(玩家.uid,文本)
            玩家.當前事件=公共事件.撈魚1()
            玩家.當前事件.玩家 = 玩家
            await 玩家.當前事件.執行事件()
            玩家.鎖 = False
        if(msg=="2"):
            玩家.鎖 = True
            文本 = "你決定裝一些水。"
            await messageReply.send(玩家.uid,文本)
            玩家.背包.增加物品(item.水(),1)
            玩家.當前事件=None
            玩家.鎖 = False
        if(msg=="3"):
            玩家.鎖 = True
            文本 = "你決定泛舟而下。"
            await messageReply.send(玩家.uid,文本)
            玩家.當前事件=晴陽森林_找到溪流_泛舟()
            玩家.當前事件.玩家 = 玩家
            await 玩家.當前事件.執行事件()
            玩家.鎖 = False
class 晴陽森林_找到溪流_泛舟(event.事件):
    def __init__(self):
        self.名稱 = "晴陽森林_找到溪流_泛舟"
    async def 執行事件(self):
        玩家 =self.玩家
        隨機數 = random.random()
        if(隨機數>=0 and 隨機數<0.3):
            文本 = "你溺水了，但沒人想救你。\n"
            傷害 = (int)(玩家.人物.血量上限*0.1)
            玩家.人物.受到不致死傷害(傷害,None)
            文本 += "你受到了 "+str(傷害)+" 點傷害。"
            await messageReply.send(玩家.uid,文本)
            玩家.當前事件=None
        if(隨機數>=0.3 and 隨機數<0.6):
            文本 = "你看到了逆流而上的魚，使得你受到了一些啟發。\n"
            await message.channel.send(文本)
            文本 = "因此你決定去選總統(誤)"
            await 玩家.人物.獲得經驗(5)
            await messageReply.send(玩家.uid,文本)
            ##TODO 經驗
            玩家.當前事件=None
        if(隨機數>=0.6 and 隨機數<=1):
            文本 = "在發現你的舟開始搖晃了起來，前方一個巨大的漩渦正在等著你。\n"
            文本 = "你要如何面對?\n"
            文本 += "=====================================" + "\n"
            文本 += "1:跳舟" + "\n"
            文本 += "2:衝進去" + "\n"
            文本 += "=====================================" + "\n"
            await messageReply.send(玩家.uid,文本)
            玩家.當前事件=晴陽森林_找到溪流_泛舟_遭遇漩渦()
            玩家.當前事件.玩家 = 玩家
            
class 晴陽森林_找到溪流_泛舟_遭遇漩渦(event.事件):
    def __init__(self):
        self.名稱 = "晴陽森林_找到溪流_泛舟_遭遇漩渦"
    async def 處理事件(self,message):
        玩家 = self.玩家
        msg = message.content.lower()
        if(玩家.鎖):
            msg = None
        if(msg=="1"):
            玩家.鎖 = True
            文本 = "你從舟上跳了下來，跳回了溪邊，還順手摸到了一隻蝦。"
            await messageReply.send(玩家.uid,文本)
            玩家.背包.增加物品(item.蝦(),1)
            玩家.當前事件=None
            玩家.鎖 = False
        if(msg=="2"):
            玩家.鎖 = True
            文本 = "你決定挑戰這個漩渦。"
            await messageReply.send(玩家.uid,文本)
            文本 = "隨著愈接近漩渦，你覺得你的身體被劇烈的撕裂，你暈了過去。"
            await messageReply.send(玩家.uid,文本)
            傷害 = (int)(玩家.人物.血量上限*0.3)
            玩家.人物.受到不致死傷害(傷害,None)
            文本 = "你受到了 "+str(傷害)+" 點傷害。"
            await messageReply.send(玩家.uid,文本)
            隨機數 = random.random()
            if(隨機數>=0.0 and 隨機數<=0.75):
                文本 = "當你醒了過來時，你已經回到了岸上。\n"
                await messageReply.send(玩家.uid,文本)
            if(隨機數>0.75 and 隨機數<=1.0):
                文本 = "當你醒了過來時，你已經回到了岸上，你的手中還握著一顆淡藍色的寶石。\n"
                await messageReply.send(玩家.uid,文本)
                玩家.背包.增加物品(item.漩渦寶石(),1)
                玩家.當前事件=None
            玩家.當前事件=None
            玩家.鎖 = False

#被樹枝絆倒
class 晴陽森林_被樹枝絆倒(event.事件):
    def __init__(self):
        self.名稱 = "晴陽森林_被樹枝絆倒"
    async def 執行事件(self):
        玩家 = self.玩家
        文本 = "你在森林中悠閒的走著，享受著森林的香氣。\n"
        文本 += "但你一個不注意，被樹枝絆倒，摔倒在地上"
        await messageReply.send(玩家.uid,文本)
        傷害 = (int)(玩家.人物.血量上限*0.2)
        玩家.人物.受到不致死傷害(傷害,None)
        文本 = "你受到了 "+str(傷害)+" 點傷害。"
        await messageReply.send(玩家.uid,文本)
        隨機數 = random.random()
        if(隨機數>=0 and 隨機數<0.35):
            文本 = "你在地上發現了一些曼陀羅花種子。\n"
            await messageReply.send(玩家.uid,文本)
            玩家.背包.增加物品(item.曼陀羅花種子(),3)
            玩家.當前事件=None
        if(隨機數>=0.35 and 隨機數<0.7):
            文本 = "你發現你身旁的草叢裡，有著閃閃發光的東西。\n"
            文本 += "你伸手將這堆東西拿了出來，竟然是金幣。\n"
            文本 += "你獲得了 100 金幣"
            await messageReply.send(玩家.uid,文本)
            玩家.獲得金幣(100)
            玩家.當前事件=None
        if(隨機數>=0.7 and 隨機數<=1):
            文本 = "你摔倒所發出的聲響，驚動了在附近的野豬。\n"
            文本+="兩隻野豬朝你衝了過來"
            await messageReply.send(玩家.uid,文本)
            野豬A = NEWRPG.野豬("野豬A")
            野豬B = NEWRPG.野豬("野豬B")
            F = NEWRPG.戰鬥()
            F.玩家 = 玩家
            玩家.戰場 = F
            玩家.人物.戰場 = F
            野豬A.戰場 =F
            野豬B.戰場 =F
            野豬A.玩家 =玩家
            野豬B.玩家 =玩家
            F.玩家方.append(玩家.人物)
            F.所有玩家方.append(玩家.人物)
            F.怪物方.append(野豬A)
            F.怪物方.append(野豬B)
            F.玩家 = 玩家
            await F.回合開始()
            
#找到營火
class 晴陽森林_找到營火(event.事件):
    def __init__(self):
        self.名稱 = "晴陽森林_找到營火"
    async def 執行事件(self):
        玩家 = self.玩家
        文本 = "=====================================" + "\n"
        文本 += "你看到遠處，燃起了白煙。" + "\n"
        文本 += "你朝煙逐漸走進，一團熾熱的紅色營火出現在你眼前。" + "\n"
        文本 += "你想要做甚麼" + "\n"
        文本 += "=====================================" + "\n"
        文本 += "1:休息" + "\n"
        文本 += "2:烤些東西" + "\n"
        文本 += "3:將營火澆熄 - 需要很多水" + "\n"
        文本 += "=====================================" + "\n"
        await messageReply.send(玩家.uid,文本)
    async def 處理事件(self,message):
        msg =message.content.lower()
        玩家 = self.玩家
        if(玩家.鎖):
            msg = None
        if(msg=="1"):
            玩家.鎖 = True
            回復量 = (int)(玩家.人物.血量上限*0.33)
            文本 = "你在營火旁邊坐了下來，體力逐漸回復。\n"
            文本+= "你回復了 "+str(回復量) + "點血量"
            玩家.人物.回復生命(回復量,None)
            await messageReply.send(玩家.uid,文本)
            玩家.當前事件=None
            玩家.鎖 = False
        if(msg=="2"):
            玩家.鎖 = True
            文本 = "你肚子餓了，你想烤點東西來吃。"
            await messageReply.send(玩家.uid,文本)
            玩家.當前事件 = 公共事件.烤東西()
            玩家.當前事件.玩家 = 玩家
            await 玩家.當前事件.執行事件()
            玩家.鎖 = False
        if(msg=="3"):
            玩家.鎖 = True
            if(玩家.背包.持有該物品('水')[0]):
                玩家.鎖 = False
                玩家.當前事件 = 晴陽森林_找到營火_將營火澆熄()
                玩家.當前事件.玩家 = 玩家
                await 玩家.當前事件.執行事件()
            else:
                文本 = "你沒有水。"
                玩家.鎖 = False
                await messageReply.send(玩家.uid,文本)
            
            
   
#找到營火_將營火澆熄
class 晴陽森林_找到營火_將營火澆熄(event.事件):
    def __init__(self):
        self.名稱 = "晴陽森林_找到營火_將營火澆熄"
    async def 執行事件(self):
        玩家 = self.玩家
        玩家.背包.減少物品("水",1)
        文本 = "=====================================" + "\n"
        文本 += "你將水倒進了營火，木炭發出了嘶嘶聲響，白煙竄上了天空。" + "\n"
        隨機數 = random.random()
        if(隨機數<0.05):
            文本 += "營火熄滅了\n"
            文本 += "你在營火當中發現到一份文件，這份文件已經被燒毀了一大半。\n"
            玩家.背包.增加物品(item.被營火燒毀的文件(),1)
            玩家.當前事件=None
            await messageReply.send(玩家.uid,文本)
        else:
            隨機數 = random.random()
            if(隨機數>=0 and 隨機數 <0.33):
                文本 +="火勢依舊旺盛，沒有熄滅的跡象。"
            if(隨機數>=0.33 and 隨機數 <0.66):
                文本 +="火勢似乎衰退了一點。"
            if(隨機數>=0.66 and 隨機數 <=1):
                文本 +="火勢似乎快熄滅了。"
            文本+="你要繼續倒水嗎?\n"
            文本+="1:繼續\n"
            文本+="c:放棄\n"
            await messageReply.send(玩家.uid,文本)
    async def 處理事件(self,message):
        msg =message.content.lower()
        玩家 = self.玩家
        if(玩家.鎖):
            msg = None
        if(msg=="1"):
            玩家.鎖 = True
            if(玩家.背包.持有該物品('水')[0]):
                await 玩家.當前事件.執行事件()
            else:
                文本 = "你沒有水了。"
                await messageReply.send(玩家.uid,文本)
            玩家.鎖 = False
        if(msg=="c"):
            文本 = "你打消了熄滅營火的念頭"
            await messageReply.send(玩家.uid,文本)
            玩家.當前事件=None
            玩家.鎖 = False
            
            
#找到神木
class 晴陽森林_找到神木(event.事件):
    def __init__(self):
        self.名稱 = "晴陽森林_找到神木"
    async def 執行事件(self):
        玩家 = self.玩家
        文本 = "=====================================" + "\n"
        文本 += "隨著愈接近森林深處，周圍的樹木也愈加的高聳。" + "\n"
        文本 += "一顆高聳參天的神木出現在你的眼前。" + "\n"
        文本 += "你想要做甚麼" + "\n"
        文本 += "=====================================" + "\n"
        文本 += "1:依著藤蔓爬上神木" + "\n"
        #文本 += "2:在神木下仰望" + "\n"
        文本 += "=====================================" + "\n"
        await messageReply.send(玩家.uid,文本)
    async def 處理事件(self,message):
        msg =message.content.lower()
        玩家 = self.玩家
        if(玩家.鎖):
            msg = None
        if(msg=="1"):
            玩家.鎖 = True
            隨機數 = random.random()
            if(隨機數>=0 and 隨機數 <0.2):
                文本 = "你發現樹上長著一顆金黃色的果實，你將它摘了下來。\n"
                await messageReply.send(玩家.uid,文本)
                玩家.背包.增加物品(item.金黃果實(),1)
            if(隨機數>=0.2 and 隨機數<0.4):
                傷害 = (int)(玩家.人物.血量上限*random.uniform(0.1,0.3))
                文本 = "你沒抓好藤蔓，從樹上摔了下來。\n"
                文本 += "受到了"+str(傷害)+"點傷害\n"
                await messageReply.send(玩家.uid,文本)
                玩家.人物.受到不致死傷害(傷害,None)
            if(隨機數>=0.4 and 隨機數<=0.6):
                文本 = "你在樹上發現到了一個鳥巢。\n"
                文本+="一隻飛鳥朝你襲擊而來\n"
                await messageReply.send(玩家.uid,文本)
                飛鳥 = NEWRPG.飛鳥("飛鳥","空")
                F = NEWRPG.戰鬥()
                F.玩家 = 玩家
                玩家.戰場 = F
                玩家.人物.戰場 = F
                飛鳥.戰場 =F
                飛鳥.玩家 =玩家
                F.玩家方.append(玩家.人物)
                F.所有玩家方.append(玩家.人物)
                F.怪物方.append(飛鳥)
                F.玩家 = 玩家
                await F.回合開始()
            if(隨機數>0.6 and 隨機數 <=0.9):
                文本 = "你沿途折了一些樹枝。\n"
                await messageReply.send(玩家.uid,文本)
                玩家.背包.增加物品(item.樹枝(),3)
            if(隨機數>0.9 and 隨機數 <=1):
                文本 = "你發現到一柄木劍，插在了神木身上，你將其拔了出來。\n"
                await messageReply.send(玩家.uid,文本)
                #玩家.背包.增加物品(item.神木樹枝(),1)
                玩家.背包.增加物品(equip.木劍())
            玩家.當前事件=None
            玩家.鎖 = False
                

import event
import messageReply
import item
import equip
import random
import NEWRPG
import globals
import 公共事件
import time
import datetime

#風鳴山洞


class 風鳴山洞_進入風鳴山洞(event.事件):
    def __init__(self):
        self.名稱 = "風鳴山洞_進入風鳴山洞"
    async def 執行事件(self):
        玩家 = self.玩家
        文本 = "=====================================" + "\n"
        文本 += "你找到了一座山洞，山洞裡不斷傳來風的聲音。" + "\n"
        文本 += "你想要做甚麼" + "\n"
        文本 += "=====================================" + "\n"
        文本 += "1:進入山洞" + "\n"
        文本 += "2:離開山洞" + "\n"
        文本 += "=====================================" + "\n"
        await messageReply.send(玩家.uid,文本)
    async def 處理事件(self,message):
        msg =message.content.lower()
        玩家 = self.玩家
        if(玩家.鎖):
            msg = None
        if(msg=="1"):
            隨機數 = random.random()
            if(隨機數>=0 and 隨機數<0.3):
                玩家.鎖 = True
                文本 = "你在山洞的陰暗處撿到了一些憤怒蘑菇。"
                await messageReply.send(玩家.uid,文本)
                玩家.背包.增加物品(item.憤怒蘑菇(),2)
                玩家.當前事件=None
                玩家.鎖 = False
            if(隨機數>=0.3 and 隨機數<0.6):
                玩家.當前事件 = 風鳴山洞_找到礦脈()
                玩家.當前事件.玩家 = 玩家
                await 玩家.當前事件.執行事件()
            if(隨機數>=0.6 and 隨機數<0.9):
                數量 = random.randint(1, 2)
                文本 = "不遠處傳來了聲音，突然間 " + str(數量) +" 隻刺蝟出現在你眼前。\n"
                await messageReply.send(玩家.uid,文本)
                F = NEWRPG.戰鬥()
                F.玩家 = 玩家
                玩家.戰場 = F
                玩家.人物.戰場 = F
                
                if(數量>=1):
                    刺蝟A=NEWRPG.刺蝟("刺蝟A")
                    刺蝟A.戰場 =F
                    刺蝟A.玩家 = 玩家
                    F.怪物方.append(刺蝟A)
                if(數量>=2):
                    刺蝟B=NEWRPG.刺蝟("刺蝟B")
                    刺蝟B.戰場 =F
                    刺蝟B.玩家 = 玩家
                    F.怪物方.append(刺蝟B)
                F.玩家方.append(玩家.人物)
                F.所有玩家方.append(玩家.人物)
                F.玩家 = 玩家
                await F.回合開始()
                玩家.當前事件 = None
            if(隨機數>=0.9 and 隨機數<1.0):
                文本 = "你遇到了一隻巨大的地鼠，他似乎察覺到了你，鑽進了地洞。\n"
                文本 += "滿地的坑洞，你要想辦法將地鼠王尋找出來。\n"
                await messageReply.send(玩家.uid,文本)
                標記列表 = ["地鼠王","地鼠","炸藥","空"]
                random.shuffle(標記列表)
                #
                F = NEWRPG.戰鬥()
                F.玩家 = 玩家
                玩家.戰場 = F
                玩家.人物.戰場 = F
                地鼠王=NEWRPG.地鼠王("地鼠王")
                地鼠王.戰場 =F
                地鼠王.玩家 = 玩家
                for a in 標記列表:
                    地洞=NEWRPG.地洞("地洞",a)
                    地洞.戰場 =F
                    地洞.玩家 = 玩家
                    if(a=="地鼠王"):
                        F.怪物_地鼠王 = 地鼠王
                    F.怪物方.append(地洞)
                F.玩家方.append(玩家.人物)
                F.所有玩家方.append(玩家.人物)
                F.玩家 = 玩家
                await F.回合開始()
                玩家.當前事件 = None
                await messageReply.send(玩家.uid,文本)            
        if(msg=="2"):
            文本 = "你離開了山洞"
            await messageReply.send(玩家.uid,文本)
            玩家.當前事件=None
            玩家.鎖 = False
        
class 風鳴山洞_找到礦脈(event.事件):
    def __init__(self):
        self.名稱 = "風鳴山洞_找到礦脈"
        self.時間 = None
    async def 執行事件(self):
        玩家 = self.玩家
        文本 = "=====================================" + "\n"
        文本 += "你找到了一處礦脈。" + "\n"
        文本 += "你想要做甚麼" + "\n"
        文本 += "=====================================" + "\n"
        文本 += "1:挖礦(需要1分鐘)" + "\n"
        文本 += "c:離開" + "\n"
        文本 += "=====================================" + "\n"
        await messageReply.send(玩家.uid,文本)
    async def 處理事件(self,message):
        msg =message.content.lower()
        玩家 = self.玩家
        if(玩家.鎖):
            msg = None
        if(msg=="1" and self.時間==None):
            self.時間 = datetime.datetime.now() + datetime.timedelta(minutes=1)
            文本 = "你開始挖礦\n"
            await messageReply.send(玩家.uid,文本)
        elif(msg=="c"):
            文本 = "你離開了礦脈\n"
            await messageReply.send(玩家.uid,文本)
            玩家.當前事件=None
            玩家.鎖 = False
        else:
            尚須 =(self.時間-datetime.datetime.now()).total_seconds()
            if(尚須>0):
                文本 = "你正在挖礦中...尚須 "+str(int(尚須)) + "秒\n"
                文本 += "c:取消挖礦\n"
                await messageReply.send(玩家.uid,文本)
            else:
                文本 = "挖礦完成"
                數量 = random.randint(1, 2)
                文本 += "你從礦脈中挖到了 " + str(數量) + " 個銅礦石。\n"
                玩家.背包.增加物品(item.銅礦石(),數量)
                數量 = random.randint(0, 1)
                if(數量!=0):
                    文本 += "以及 " + str(數量) + " 個焦炭。\n"
                    玩家.背包.增加物品(item.焦炭(),數量)
                玩家.當前事件=None
                玩家.鎖 = False
                await messageReply.send(玩家.uid,文本)
                
                
            

import discord
from discord.ext import commands
import item
import equip
import random
import NEWRPG
import skill
import asyncio

import messageReply
import tokens
import globals
import event
import 晴陽森林事件
import 霜雪山丘事件
import 風鳴山洞事件

print(discord.__version__)
bot = commands.Bot(command_prefix='!')

test = 0
#

async def 每30秒():
    while True:
        print("30秒")
        #globals.pint()
        #globals.save()
        await asyncio.sleep(30)

#




#
class 地形:
    def __init__(self):
        self.名稱 = None
        self.事件列表 = list()
        self.機率列表 = list()
    def 探索(self):
        隨機數 = random.random()
        上限 = 0
        下限 = 0
        for a in range(len(self.事件列表)):
            上限= self.機率列表[a]
            if(隨機數>=下限 and 隨機數 <上限):
                return self.事件列表[a]
            else:
                下限 = self.機率列表[a]
        
        


#測試事件
class 測試事件(event.事件):
    def __init__(self):
        self.名稱 = "測試事件"
    async def 執行事件(self):
        玩家 = self.玩家
        #玩家.當前事件 = 霜雪山丘_進入霜雪山丘_狀態1()
    

         
        '''
        文本 = "你發現到了四隻蝙蝠。\n"
        await messageReply.send(玩家.uid,文本)
        蝙蝠A = NEWRPG.蝙蝠("蝙蝠A")
        蝙蝠B = NEWRPG.蝙蝠("蝙蝠B")
        蝙蝠C = NEWRPG.蝙蝠("蝙蝠C")
        蝙蝠D = NEWRPG.蝙蝠("蝙蝠D")
        F = NEWRPG.戰鬥()
        F.玩家 = 玩家
        玩家.戰場 = F
        玩家.人物.戰場 = F
        蝙蝠A.戰場 =F
        蝙蝠A.玩家 =玩家
        蝙蝠B.戰場 =F
        蝙蝠B.玩家 =玩家
        蝙蝠C.戰場 =F
        蝙蝠C.玩家 =玩家
        蝙蝠D.戰場 =F
        蝙蝠D.玩家 =玩家
        F.玩家方.append(玩家.人物)
        F.所有玩家方.append(玩家.人物)
        F.怪物方.append(蝙蝠A)
        F.怪物方.append(蝙蝠B)
        F.怪物方.append(蝙蝠C)
        F.怪物方.append(蝙蝠D)
        F.玩家 = 玩家
        await F.回合開始()
        
        
        文本 = "你發現到了一隻企鵝。\n"
        await messageReply.send(玩家.uid,文本)
        企鵝B = NEWRPG.企鵝("玩家的企鵝")
        雪原狼A=NEWRPG.地鼠("地鼠A")
        雪原狼B=NEWRPG.地鼠("地鼠B")
        F = NEWRPG.戰鬥()
        F.玩家 = 玩家
        玩家.戰場 = F
        玩家.人物.戰場 = F
        企鵝B.戰場 =F
        企鵝B.玩家 =玩家
        雪原狼A.戰場 =F
        雪原狼A.玩家 = 玩家
        雪原狼B.戰場 =F
        雪原狼B.玩家 = 玩家
 
        F.玩家方.append(玩家.人物)
        F.玩家方.append(企鵝B)
        F.怪物方.append(雪原狼A)
        F.怪物方.append(雪原狼B)
        F.所有玩家方.append(玩家.人物)
        F.玩家 = 玩家
        await F.回合開始()
        '''
       

    
#
class 晴陽森林(地形):
    def __init__(self):
        super().__init__()
        self.事件列表=[晴陽森林事件.晴陽森林_被樹枝絆倒(),晴陽森林事件.晴陽森林_找到溪流(),晴陽森林事件.晴陽森林_找到營火(),晴陽森林事件.晴陽森林_找到神木()]
        self.機率列表=[0.25,0.5,0.8,1.0]#CDF
#
class 霜雪山丘(地形):
    def __init__(self):
        super().__init__()
        self.事件列表=[霜雪山丘事件.霜雪山丘_進入霜雪山丘()]
        self.機率列表=[1.0]#CDF
#
class 風鳴山洞(地形):
    def __init__(self):
        super().__init__()
        self.事件列表=[風鳴山洞事件.風鳴山洞_進入風鳴山洞()]
        self.機率列表=[1.0]#CDF
#
        


globals.load()
bot.loop.create_task(每30秒())


@bot.event
async def on_ready():
    print('Logged in as')
    print(bot.user.name)
    print(bot.user.id)
    print('------')

  
async def start(message):
    global test
    test = message
    user = message.author
    username = user.name
    userid = user.id

async def create(message):
    user = message.author
    username = user.name
    userid = user.id
    tag = "<@"+str(userid)+"> "
    if(userid in globals.玩家列表):
        文本 = "你已經擁有角色，輸入 /info 查看人物訊息。"
    else:
        新玩家 = NEWRPG.玩家()
        玩家人物 = NEWRPG.玩家角色(新玩家)
        玩家人物.名稱 = username
        玩家人物.玩家=新玩家
        新玩家.人物 = 玩家人物
        新玩家.uid=userid
        新玩家.名稱 = username
        globals.玩家列表[userid]=新玩家
        '''
        新玩家.背包.增加物品(equip.粗糙的狼毫毛靴(伺服器全局資訊["裝備編號"]),1)
        伺服器全局資訊["裝備編號"] += 1
        新玩家.背包.增加物品(equip.粗糙的鳥羽毛帽(伺服器全局資訊["裝備編號"]),1)
        伺服器全局資訊["裝備編號"] += 1
        新玩家.背包.增加物品(equip.粗糙的豬皮手套(伺服器全局資訊["裝備編號"]),1)
        伺服器全局資訊["裝備編號"] += 1
        新玩家.背包.增加物品(equip.企鵝牌防寒衣(伺服器全局資訊["裝備編號"]),1)
        伺服器全局資訊["裝備編號"] += 1
        
        #新玩家.背包.增加物品(item.雪原狼的爪子(),7)
        #新玩家.背包.增加物品(item.烤豬肉(),5)
        '''
        #新玩家.背包.增加物品(item.憤怒蘑菇(),2)
        #新玩家.背包.增加物品(equip.木劍())
        #新玩家.背包.增加物品(equip.狼牙棒())
        文本 = "角色創立成功，輸入 /info 查看人物訊息。"
    await message.channel.send(tag+ 文本)

def getPlayerInfoEmbed(玩家人物):
    embed = discord.Embed(title="玩家資訊", description=玩家人物.名稱, color=0x00ff00)
    embed.add_field(name="等級", value="{0}".format(str(玩家人物.等級)), inline=True)
    embed.add_field(name="經驗", value="{0}/{1}".format(str(玩家人物.經驗),str(玩家人物.升級所需經驗)), inline=True)
    embed.add_field(name="血量", value="{0}/{1}".format(str(玩家人物.當前血量),str(玩家人物.血量上限)), inline=True)
    embed.add_field(name="魔力", value="{0}/{1}".format(str(玩家人物.當前魔力),str(玩家人物.魔力上限)), inline=True)
    embed.add_field(name="速度", value="{0}(+{1})".format(str(玩家人物.速度[0]),str(玩家人物.速度[1])), inline=True)
    embed.add_field(name="物理攻擊", value="{0}(+{1})".format(str(玩家人物.物理攻擊[0]),str(玩家人物.物理攻擊[1])), inline=True)
    embed.add_field(name="物理防禦", value="{0}(+{1})".format(str(玩家人物.物理防禦[0]),str(玩家人物.物理防禦[1])), inline=True)
    embed.add_field(name="魔法攻擊", value="{0}(+{1})".format(str(玩家人物.魔法攻擊[0]),str(玩家人物.魔法攻擊[1])), inline=True)
    embed.add_field(name="魔法防禦", value="{0}(+{1})".format(str(玩家人物.魔法防禦[0]),str(玩家人物.魔法防禦[1])), inline=True)
    embed.add_field(name="命中", value="{0}(+{1})".format(str(玩家人物.命中[0]),str(玩家人物.命中[1])), inline=True)
    embed.add_field(name="閃避", value="{0}(+{1})".format(str(玩家人物.閃避[0]),str(玩家人物.閃避[1])), inline=True)
    embed.add_field(name="爆擊", value="{0}(+{1})".format(str(玩家人物.爆擊[0]),str(玩家人物.爆擊[1])), inline=True)
    embed.add_field(name="屬性攻擊力", value="雷:{0}(+{1}) 火:{2}(+{3}) 冰:{4}(+{5}) 地:{6}(+{7}) 光:{8}(+{9}) 暗:{10}(+{11})".format(str(玩家人物.雷屬性攻擊力[0]),str(玩家人物.雷屬性攻擊力[1]),str(玩家人物.火屬性攻擊力[0]),str(玩家人物.火屬性攻擊力[1]),str(玩家人物.冰屬性攻擊力[0]),str(玩家人物.冰屬性攻擊力[1]),str(玩家人物.地屬性攻擊力[0]),str(玩家人物.地屬性攻擊力[1]),str(玩家人物.光屬性攻擊力[0]),str(玩家人物.光屬性攻擊力[1]),str(玩家人物.暗屬性攻擊力[0]),str(玩家人物.暗屬性攻擊力[1])), inline=True)
    embed.add_field(name="屬性防禦力", value="雷:{0}(+{1}) 火:{2}(+{3}) 冰:{4}(+{5}) 地:{6}(+{7}) 光:{8}(+{9}) 暗:{10}(+{11})".format(str(玩家人物.雷屬性防禦力[0]),str(玩家人物.雷屬性防禦力[1]),str(玩家人物.火屬性防禦力[0]),str(玩家人物.火屬性防禦力[1]),str(玩家人物.冰屬性防禦力[0]),str(玩家人物.冰屬性防禦力[1]),str(玩家人物.地屬性防禦力[0]),str(玩家人物.地屬性防禦力[1]),str(玩家人物.光屬性防禦力[0]),str(玩家人物.光屬性防禦力[1]),str(玩家人物.暗屬性防禦力[0]),str(玩家人物.暗屬性防禦力[1])), inline=True)
    return embed

async def info(message):
    user = message.author
    username = user.name
    userid = user.id
    tag = "<@"+str(userid)+"> "
    玩家人物 = globals.玩家列表[userid].人物
    embed = getPlayerInfoEmbed(玩家人物)
    await message.channel.send(tag,embed=embed)
    
async def bag(message):
    user = message.author
    username = user.name
    userid = user.id
    玩家 = globals.玩家列表[userid]
    tag = "<@"+str(userid)+">"
    文本 = "的背包\n"
    文本 += 玩家.背包.背包資訊()
    文本 += "\n 金幣:" + str(玩家.金幣)
    文本 += "\n 使用 /iinfo [物品名稱] 查看詳細資訊"
    文本 += "\n 使用 /use [物品名稱] [數量] 來使用物品。"
    文本 += "\n 使用 /use [裝備編號]  來裝上裝備。"
    print(文本)
    await message.channel.send(tag+文本)

async def equipq(message):
    user = message.author
    username = user.name
    userid = user.id
    玩家 = globals.玩家列表[userid]
    tag = "<@"+str(userid)+">"
    文本 = "的裝備\n"
    文本 += 玩家.人物.裝備.裝備資訊()
    文本 += "\n 使用 /einfo [裝備欄位號碼] 查看詳細資訊"
    文本 += "\n 使用 /unload [裝備欄位號碼] 來卸下裝備。"
    print(文本)
    await message.channel.send(tag+文本)

async def skillq(message):
    user = message.author
    username = user.name
    userid = user.id
    玩家 = globals.玩家列表[userid]
    tag = "<@"+str(userid)+">"
    文本 = "學會的技能\n"
    for a in range(len(玩家.人物.技能列表)):
        文本+=str(a)+":"+玩家.人物.技能列表[a].名稱+" 耗魔: "+str(玩家.人物.技能列表[a].消耗魔力)+" 效果: "+玩家.人物.技能列表[a].描述+"\n"
    print(文本)
    await message.channel.send(tag+文本)

async def helpq(message):
    user = message.author
    username = user.name
    userid = user.id
    玩家 = globals.玩家列表[userid]
    文本 = "/create 創建人物\n"
    文本 += "/info 查看人物資訊\n"
    文本 += "/skill 查看技能\n"
    文本 += "/bag 查看背包\n"
    文本 += "/equip 查看裝備\n"
    文本 += "/山丘 探索山丘\n"
    文本 += "/山洞 探索山洞\n"
    文本 += "/森林 探索森林\n"
    await message.channel.send(文本)
    
async def item_info(message):
    msg = message.content.lower()
    user = message.author
    username = user.name    
    userid = user.id
    物品名稱 = msg.split()[1]
    玩家 = globals.玩家列表[userid]
    該物品 = 玩家.背包.持有該物品(物品名稱)[1]
    tag = "<@"+str(userid)+">\n"
    if(該物品==False):
        文本 = "查無該物品資訊"
    else:
        文本 = 該物品.物品資訊()
    await message.channel.send(tag+文本)

async def equip_info(message):
    msg = message.content.lower()
    user = message.author
    username = user.name    
    userid = user.id
    裝備欄位編號 = int(msg.split()[1])
    玩家 = globals.玩家列表[userid]
    人物 = 玩家.人物
    裝備 = 人物.裝備.裝備列表[人物.裝備.裝備欄位[裝備欄位編號]]
    tag = "<@"+str(userid)+">\n"
    if(裝備==None):
        文本 = "查無該裝備資訊"
    else:
        文本 = 裝備.物品資訊()
    await message.channel.send(tag+文本)

async def equip_unload(message):
    msg = message.content.lower()
    user = message.author
    username = user.name    
    userid = user.id
    裝備欄位編號 = int(msg.split()[1])
    玩家 = globals.玩家列表[userid]
    人物 = 玩家.人物
    文本 = 人物.裝備.卸下裝備(人物.裝備.裝備欄位[裝備欄位編號])
    await message.channel.send(文本)
    
async def item_use(message):
    msg = message.content.lower()
    user = message.author
    username = user.name
    userid = user.id
    ms = msg.split()
    if(len(ms)>=2):
        物品名稱 = ms[1]
    else:
        物品名稱 = ""
    if(len(ms)>=3):
        數量 = int(ms[2])
    else:
        數量 = 1
    玩家 = globals.玩家列表[userid]
    該物品 = 玩家.背包.持有該物品(物品名稱)[1]
    tag = "<@"+str(userid)+">\n"
    if(該物品==False):
        文本 = "你沒有該物品"
        await message.channel.send(tag+文本)
    else:
        await 該物品.使用物品(玩家,數量)
   
    
async def 找到晴陽森林(message):  
    user = message.author
    username = user.name
    userid = user.id
    玩家 = globals.玩家列表[userid]
    tag = "<@"+str(userid)+"> "
    文本 = "你來到了晴陽森林"
    await message.channel.send(tag+ 文本)
    晴陽 = 晴陽森林()
    事件 = 晴陽.探索()
    玩家.當前事件=事件
    事件.玩家 = 玩家
    await 事件.執行事件()

async def 找到霜雪山丘(message):  
    user = message.author
    username = user.name
    userid = user.id
    玩家 = globals.玩家列表[userid]
    tag = "<@"+str(userid)+"> "
    文本 = "你來到了霜雪山丘"
    await message.channel.send(tag+ 文本)
    霜雪 = 霜雪山丘()
    事件 = 霜雪.探索()
    玩家.當前事件=事件
    事件.玩家 = 玩家
    await 事件.執行事件()

async def 找到風鳴山洞(message):  
    user = message.author
    username = user.name
    userid = user.id
    玩家 = globals.玩家列表[userid]
    tag = "<@"+str(userid)+"> "
    文本 = "你來到了風鳴山洞"
    await message.channel.send(tag+ 文本)
    風鳴 = 風鳴山洞()
    事件 = 風鳴.探索()
    玩家.當前事件=事件
    事件.玩家 = 玩家
    await 事件.執行事件()
    
@bot.event
async def on_reaction_add(reaction, user):
    if(str(user.id)!="567876825347522570"):
        print(control[str(reaction)],user)
        await reaction.message.edit(content=control[str(reaction)])
        await reaction.message.clear_reactions()
        for key,v in control.items():
            await reaction.message.add_reaction(key)

control ={
            "\U00002B06": "上",
            "\U00002B07": "下",
            "\U00002B05": "左",
            "\U000027A1": "右",
        }

@bot.event
async def on_message(message):
    #print(message)
    msg = message.content.lower()
    user = message.author
    username = user.name
    userid = user.id
    messageReply.set_message(userid,message)
    tag = "<@"+str(userid)+">"
    if(str(userid)!="567876825347522570"):
        print(username,userid,msg)
        if msg=="/create":
            await create(message)
        if(userid not in globals.玩家列表 and msg[0]=="/"):
            文本 = "你尚未擁有角色，輸入 /create 創造角色。"
            await message.channel.send(tag+ 文本)
        else:
            玩家 = globals.玩家列表[userid]
            當前事件 = 玩家.當前事件
            if (當前事件 is None):
                if msg=="/start":
                    await start(message)
                if msg=="/info":
                    await info(message)
                if msg=="/bag":
                    await bag(message)
                if msg=="/equip":
                    await equipq(message)
                if msg=="/skill":
                    await skillq(message)
                if msg=="/help":
                    await helpq(message)
                if msg.startswith("/iinfo"):
                    await item_info(message)
                if msg.startswith("/einfo"):
                    await equip_info(message)
                if msg.startswith("/unload"):
                    await equip_unload(message)
                if msg.startswith("/use"):
                    await item_use(message)
                if msg=="/森林":
                    await 找到晴陽森林(message)
                if msg=="/山丘":
                    await 找到霜雪山丘(message)
                if msg=="/山洞":
                    await 找到風鳴山洞(message)
                if msg=="/test":
                    '''
                    def check(r, u):
                        if str(r) not in control.keys():
                            return False
                        return True
                    k = await message.channel.send("123")
                    for key,v in control.items():
                          await k.add_reaction(key)
                    reaction, user = await bot.wait_for('reaction_add', timeout=60.0,check=check)
                    '''
                    玩家.當前事件=霜雪山丘事件.霜雪山丘_進入霜雪山丘_狀態1()
                    玩家.當前事件.玩家=玩家
                    await 玩家.當前事件.執行事件()
                    
            else:
                await 當前事件.處理事件(message)
            if (玩家.戰場!=None):
                if 玩家.戰場.狀態 == "逃跑":
                    文本 = "---戰鬥結束---"
                    玩家.戰場 = None
                    #TODO戰鬥結束後處理
                    玩家.當前事件 = None
                    await message.channel.send(tag+ 文本)
                elif (玩家.戰場.戰鬥結束):
                    文本 = "---戰鬥結束---\n"
                    print("狀態:"+玩家.戰場.狀態)
                    if(玩家.戰場.狀態 == "玩家方勝利"):
                        玩家.戰場.狀態 = "已結算"
                        文本+="---戰利品---\n"
                        for a in 玩家.戰場.戰利品:
                            物品 = a()
                            玩家.背包.增加物品(物品,1)
                            文本+=str(物品.名稱) + "X 1\n"
                        玩家.獲得金幣(玩家.戰場.金錢獎勵)
                        文本+="金幣:"+str(玩家.戰場.金錢獎勵)+"\n"
                        文本+="經驗:"+str(玩家.戰場.經驗獎勵)+"\n"
                        await 玩家.人物.獲得經驗(玩家.戰場.經驗獎勵)
                        await message.channel.send(tag+ 文本)
                        if(玩家.戰場.勝利事件!=None):
                            玩家.當前事件=玩家.戰場.勝利事件
                            玩家.當前事件.玩家=玩家
                            await 玩家.當前事件.執行事件()
                        玩家.戰場 = None
                        玩家.當前事件 = None
                    elif(玩家.戰場.狀態 == "怪物方勝利"):
                        玩家.戰場.狀態 = "已結算"
                        #TODO 死亡懲罰
                        玩家.人物.當前血量 = 玩家.人物.血量上限
                        玩家.人物.存活 = True
                        await message.channel.send(tag+ 文本)
                        玩家.戰場 = None
                        玩家.當前事件 = None
                    #await message.channel.send(tag+ 文本)
                   
                elif (玩家.戰場.當前行動者 == 玩家.人物):
                    await 玩家.戰場.處理訊息(message)
                   

bot.run(tokens.getToken())


    #if message.content.startswith('!hello'):
        #embed = discord.Embed(title="Tile", description="Desc", color=0x00ff00)
        #embed.add_field(name="Field1", value="hi", inline=False)
        #embed.add_field(name="Field2", value="hi2", inline=False)
        #await message.channel.send('Hello!')
  #        file = discord.File("m1.jpg", filename="m1.jpg")
  #      await message.channel.send(tag,embed=embed,file=file)

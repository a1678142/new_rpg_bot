import messageReply
#import NEWRPG
import random
import skill
class 物品:
    def __init__(self):
        self.名稱=None
        self.說明=None
        self.類型=["雜物"]
    async def 使用物品(self,玩家,數量):
        文本 = "該物品無法使用"
        await messageReply.send(玩家.uid,文本)
    def 物品資訊(self):
        文本 = self.名稱+":"+ self.說明+"\n"
        for a in self.類型:
             文本 += "["+a+"]"
        if("裝備"in self.類型):
            文本+="\n"
            文本+=self.裝備資訊()
        return 文本
#TODO 生命藥水
class 生命藥水1(物品):
    def __init__(self):
        super().__init__()
        self.名稱="1級生命藥水"
        self.說明="想也知道生命藥水一定是紅色的。使用後恢復20點生命"

class 金黃果實(物品):
    def __init__(self):
        super().__init__()
        self.名稱="金黃果實"
        self.說明="金黃色的果實，看起來很可口，吃下去也許可以獲得一些經驗。"
        self.類型=["食物"]
    async def 使用物品(self,玩家,數量):
        if(玩家.背包.內容物[self.名稱]>=數量):
            文本 = ""
            for a in range(數量):
                玩家.背包.減少物品(self.名稱,1)
                文本 += "你將金黃果實吞了下去。\n"
                await 玩家.人物.獲得經驗(random.randint(1,10))
        else:
            文本="數量不足"
        await messageReply.send(玩家.uid,文本)

class 憤怒蘑菇(物品):
    def __init__(self):
        super().__init__()
        self.名稱="憤怒蘑菇"
        self.類型=["戰鬥"]
        self.說明="於戰鬥中使用，消耗10%生命，提升15%+3點物理攻擊力，持續一回合，使用該物品不會消耗行動。"
        self.技能=skill.憤怒蘑菇()
    async def 使用物品(self,玩家,數量):
        if(玩家.人物.戰場==None):
            文本="該物品只能於戰鬥中使用\n"
            await messageReply.send(玩家.uid,文本)

class 銅礦石(物品):
    def __init__(self):
        super().__init__()
        self.名稱="銅礦石"
        self.說明="銅礦石，可以用來製作裝備。"    

        
class 神木樹枝(物品):
    def __init__(self):
        super().__init__()
        self.名稱="神木樹枝"
        self.說明="神木的樹枝"

class 雪蓮花(物品):
    def __init__(self):
        super().__init__()
        self.名稱="雪蓮花"
        self.說明="只在寒冷中才會生長的野花，據說其汁液相當美味。"

class 樹枝(物品):
    def __init__(self):
        super().__init__()
        self.名稱="樹枝"
        self.說明="普通的樹枝。"        

class 雪原狼的爪子(物品):
    def __init__(self):
        super().__init__()
        self.名稱="雪原狼的爪子"
        self.說明="雪原狼身上的爪子，看起來十分的鋒利。"

class 刺蝟的刺(物品):
    def __init__(self):
        super().__init__()
        self.名稱="刺蝟的刺"
        self.說明="刺蝟身上的刺。"
        
#TODO 治癒咒文
class 治癒咒文(物品):
    def __init__(self):
        super().__init__()
        self.名稱="治癒咒文"
        self.說明="這個咒文有著治癒人心的能力。使用後恢復25%生命"

class 魚(物品):
    def __init__(self):
        super().__init__()
        self.名稱="魚"
        self.說明="一條看起來奄奄一息的魚。"

class 烤魚(物品):
    def __init__(self):
        super().__init__()
        self.名稱="烤魚"
        self.類型=["食物"]
        self.說明="很香的烤魚，於戰鬥外使用，下一場戰鬥前3回合會獲得命中+6。"
    async def 使用物品(self,玩家,數量):
        if(玩家.背包.內容物[self.名稱]>=數量):
            文本 = ""
            for a in range(數量):
                if(玩家.人物.含有效果("料理_命中增加")==False):
                    玩家.背包.減少物品(self.名稱,1)
                    效果 = NEWRPG.料理_命中增加效果(6,3)
                    玩家.人物.效果列表.append(效果)
                    效果.獲得效果(玩家.人物)
                    文本 += "你吃了烤魚，感覺命中提升了。\n"
                else:
                    文本 += "你已經擁有同類型的效果。\n"
        else:
            文本="數量不足"
        await messageReply.send(玩家.uid,文本)
        
            
class 焦炭(物品):
    def __init__(self):
        super().__init__()
        self.名稱="焦炭"
        self.說明="焦炭色的焦炭。"
    

class 蝦(物品):
    def __init__(self):
        super().__init__()
        self.名稱="蝦"
        self.說明="一尾看起來奄奄一息的蝦。"
    

class 烤蝦(物品):
    def __init__(self):
        super().__init__()
        self.名稱="烤蝦"
        self.類型=["食物"]
        self.說明="金黃色的鮮蝦，於戰鬥外使用，下一場戰鬥前3回合會獲得閃避+6。"
    async def 使用物品(self,玩家,數量):
        if(玩家.背包.內容物[self.名稱]>=數量):
            文本 = ""
            for a in range(數量):
                if(玩家.人物.含有效果("料理_閃避增加")==False):
                    玩家.背包.減少物品(self.名稱,1)
                    效果 = NEWRPG.料理_閃避增加效果(6,3)
                    玩家.人物.效果列表.append(效果)
                    效果.獲得效果(玩家.人物)
                    文本 += "你吃了烤蝦，感覺到閃避提升了。\n"
                else:
                    文本 += "你已經擁有同類型的效果。\n"
        else:
            文本="數量不足"
        await messageReply.send(玩家.uid,文本)


class 豬肉(物品):
    def __init__(self):
        super().__init__()
        self.名稱="豬肉"
        self.說明="只是一片豬肉。"

class 烤豬肉(物品):
    def __init__(self):
        super().__init__()
        self.名稱="烤豬肉"
        self.類型=["食物"]
        self.說明="一片烤豬肉，於戰鬥外使用，下一場戰鬥前3回合會獲得爆擊+6。"
    async def 使用物品(self,玩家,數量):
        if(玩家.背包.內容物[self.名稱]>=數量):
            文本 = ""
            for a in range(數量):
                if(玩家.人物.含有效果("料理_爆擊增加")==False):
                    玩家.背包.減少物品(self.名稱,1)
                    效果 = NEWRPG.料理_爆擊增加效果(6,3)
                    玩家.人物.效果列表.append(效果)
                    效果.獲得效果(玩家.人物)
                    文本 += "你吃了一片烤豬肉沒有配吐司，但你還是感覺到爆擊提升了。\n"
                else:
                    文本 += "你已經擁有同類型的效果。\n"
        else:
            文本="數量不足"
        await messageReply.send(玩家.uid,文本)

class 被營火燒毀的文件(物品):
     def __init__(self):
        super().__init__()
        self.名稱="被營火燒毀的文件"
        self.說明="<被營火燒毀的文件>\n"
        self.說明+="這份文件已經被燒毀了大半，你只能從上面隱約看到如下幾個字"
        self.說明+="待補"

class 水(物品):
    def __init__(self):
        super().__init__()
        self.名稱="水"
        self.說明="英國研究，地球上死亡的人，都曾喝過水。"

class 漩渦寶石(物品):
    def __init__(self):
        super().__init__()
        self.名稱="漩渦寶石"
        self.類型=["技能書"]
        self.說明="使用後可以學會 漩渦 技能。"
    async def 使用物品(self,玩家,數量):
        if(玩家.背包.內容物[self.名稱]>=數量):
            文本 = ""
            for a in range(數量):
                if(玩家.人物.含有該技能("漩渦")==False):
                    玩家.背包.減少物品(self.名稱,1)
                    文本 += "漩渦寶石在你的眼中消失，你感覺自己學習掌握漩渦的力量。\n"
                    玩家.人物.技能列表.append(skill.漩渦())
                else:
                    文本 += "你盯著寶石看，但甚麼事也沒有發生。\n"
        else:
            文本="數量不足"
        await messageReply.send(玩家.uid,文本)
    

class 曼陀羅花種子(物品):
    def __init__(self):
        super().__init__()
        self.名稱="曼陀羅花種子"
        self.說明="找到合適的地方可以種植。"

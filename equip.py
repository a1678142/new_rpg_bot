from item import 物品
import messageReply
import random
import globals

class 裝備(物品):
    def __init__(self):
        super().__init__()
        self.編號= globals.伺服器全局資訊["裝備編號"]
        globals.裝備列表[self.編號]=self
        globals.伺服器全局資訊["裝備編號"]+=1
        #
        self.物理攻擊 = 0
        self.命中 = 0
        self.爆擊 = 0
        self.閃避 = 0
        self.速度 = 0
        self.血量上限=0
        self.物理防禦=0
        self.魔法防禦=0
        self.冰屬性防禦力 = 0
        #
    async def 使用物品(self,玩家,數量):
        文本 = 玩家.人物.裝備.裝上裝備(self)
        await messageReply.send(玩家.uid,文本)
    def 裝備資訊(self):
        文本 = ""
        if(self.物理攻擊!=0):
            文本+= "物理攻擊:" + str(self.物理攻擊) + "\n"
        if(self.命中!=0):
            文本+= "命中:" + str(self.命中) + "\n"
        if(self.爆擊!=0):
            文本+= "爆擊:" + str(self.爆擊) + "\n"
        if(self.閃避!=0):
            文本+= "閃避:" + str(self.閃避) + "\n"
        if(self.速度!=0):
            文本+= "速度:" + str(self.速度) + "\n"
        if(self.血量上限!=0):
            文本+= "血量:" + str(self.血量上限) + "\n"
        if(self.物理防禦!=0):
            文本+= "物理防禦:" + str(self.物理防禦) + "\n"
        if(self.魔法防禦!=0):
            文本+= "魔法防禦:" + str(self.魔法防禦) + "\n"
        if(self.冰屬性防禦力!=0):
            文本+= "冰屬性防禦力:" + str(self.冰屬性防禦力) + "\n"
        return 文本

    
class 木劍(裝備):
    def __init__(self):
        super().__init__()
        self.名稱="木劍"
        self.說明="一柄木劍"
        self.類型=["裝備","單手武器","單手劍"]
        self.物理攻擊 = 2 + random.randint(0, 2)
        self.命中 = 3

class 銅劍(裝備):
    def __init__(self):
        super().__init__()
        self.名稱="銅劍"
        self.說明="一柄銅劍"
        self.類型=["裝備","單手武器","單手劍"]
        self.物理攻擊 = 4 + random.randint(2, 4)
        self.命中 = 6   

class 銅匕首(裝備):
    def __init__(self):
        super().__init__()
        self.名稱="青銅匕首"
        self.說明="一柄使用銅製的匕首"
        self.類型=["裝備","單手武器","匕首"]
        self.物理攻擊 = 2
        self.速度 = 2
        self.爆擊 = 4 + random.randint(0, 4)

class 狼牙棒(裝備):
    def __init__(self):
        super().__init__()
        self.名稱="狼牙棒"
        self.說明="一柄木棒，上面有許多的刺，近戰攻擊敵人時有50%機率使其產生流血8層。"
        self.類型=["裝備","雙手武器","鈍器"]
        self.物理攻擊 = 5 + random.randint(2, 3)
        self.速度 = -4
        
class 銅盾(裝備):
    def __init__(self):
        super().__init__()
        self.名稱="銅盾"
        self.說明="銅盾"
        self.類型=["裝備","盾"]
        self.物理防禦 = 5 + random.randint(0, 4)
    
        
class 銅錘(裝備):
    def __init__(self):
        super().__init__()
        self.名稱="銅錘"
        self.說明="一柄銅錘，非常的沉重。"
        self.類型=["裝備","雙手武器","雙手錘"]
        self.物理攻擊 = 10 + random.randint(4, 6)
        self.速度 = -6
        
class 企鵝牌防寒衣(裝備):
    def __init__(self):
        super().__init__()
        self.名稱="企鵝牌防寒衣"
        self.說明="一件防寒衣，在防寒衣的背後繡有著企鵝的圖案。"
        self.類型=["裝備","衣服"]
        self.物理防禦 = 3 + random.randint(0, 3)
        self.魔法防禦 = 3 + random.randint(0, 4)
        self.冰屬性防禦力 = 8 + random.randint(0, 6)

class 地鼠王盔甲(裝備):
    def __init__(self):
        super().__init__()
        self.名稱="地鼠王盔甲"
        self.說明="打贏地鼠王獲得到的盔甲，也許是其他冒險者穿過的。"
        self.類型=["裝備","衣服"]
        self.物理防禦 = 6 + random.randint(0, 8)
        self.血量上限 = 6 + random.randint(0, 5)

class 粗糙的豬皮手套(裝備):
    def __init__(self):
        super().__init__()
        self.名稱="粗糙的豬皮手套"
        self.說明="用野豬皮製成的手套，相當的粗糙。"
        self.類型=["裝備","手部"]
        self.物理防禦 = 4 + random.randint(0, 3)
        self.物理攻擊 = 3 + random.randint(0, 2)
      

class 粗糙的鳥羽毛帽(裝備):
    def __init__(self):
        super().__init__()
        self.名稱="粗糙的鳥羽毛帽"
        self.說明="用鳥羽毛裝飾的毛帽，相當的粗糙。"
        self.類型=["裝備","頭部"]
        self.速度 = 1 + random.randint(0, 2)
        self.閃避 = 2 + random.randint(0, 4)
      

class 粗糙的狼毫毛靴(裝備):
    def __init__(self):
        super().__init__()
        self.名稱="粗糙的狼毫毛靴"
        self.說明="用狼毫毛製作的靴子，相當的粗糙。"
        self.類型=["裝備","腿部"]
        self.血量上限 = 2 + random.randint(0, 4)
        self.魔法防禦 = 2 + random.randint(0, 4)

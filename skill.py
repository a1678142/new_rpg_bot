class 技能:
    def __init__(self):
        self.名稱=""
        self.描述=""
        self.消耗魔力=0
        self.當前冷卻=0
        self.冷卻回合數=0
        self.類型 = 0
class 重擊:
    def __init__(self):
        super().__init__()
        self.名稱="重擊"
        self.描述="對敵方單體造成[物理]攻擊x1.5倍物理傷害，並有機率暈眩敵人1次行動，傷害愈高機率愈高。冷卻:4回合。[近戰],[命中],[爆擊],[攻擊]"
        self.消耗魔力=0
        self.當前冷卻=0
        self.冷卻回合數=4
        self.類型 = "敵方單體"

class 冰箭:
    def __init__(self):
        super().__init__()
        self.名稱="冰箭"
        self.描述= "發射冰箭對敵方單體造成([魔法]攻擊x0.6+[冰屬性攻擊]x0.6)點傷害，並附加3層[冰緩]效果，持續3回合，冷卻2回合。若消耗[水]施放，則該次技能不會進冷卻。[遠程]"
        self.消耗魔力=30
        self.當前冷卻=0
        self.冷卻回合數=2
        self.類型 = "敵方單體"

class 活水術:
    def __init__(self):
        super().__init__()
        self.名稱="活水術"
        self.描述="獲得60點[水]。 [水]優先作為魔力消耗。若消耗[水]施放則獲得80點[水]。"
        self.消耗魔力=40
        self.當前冷卻=0
        self.冷卻回合數=1
        self.類型 = "立即施放"

class 漩渦:
    def __init__(self):
        super().__init__()
        self.名稱="漩渦"
        self.描述="對敵方全體共X個敵人造成([魔法]攻擊x0.3+[冰屬性攻擊]x0.8)點傷害。 冷卻:4+X回合 若消耗[水]使用則使敵方全體附加4層[冰緩]效果，持續2回合。"
        self.消耗魔力=60
        self.當前冷卻=0
        self.冷卻回合數=4
        self.類型 = "敵方群體"

class 召喚雪人:
    def __init__(self):
        super().__init__()
        self.名稱="召喚雪人"
        self.描述="召喚2個無法行動的雪人，來吸引敵人注意力，雪人會在3回合後融化。"
        self.消耗魔力=0
        self.當前冷卻=0
        self.冷卻回合數=5
        self.類型 = "召喚"
        
class 鬼火術:
    def __init__(self):
        super().__init__()
        self.名稱="鬼火術"
        self.描述="自身獲得一層[鬼火]狀態，最多四層。[鬼火]:提升8點[火屬性攻擊]，並於自己行動結束，對隨機敵人造成[火屬性攻擊]x0.25點傷害。[遠程]"
        self.消耗魔力=0
        self.當前冷卻=0
        self.冷卻回合數=1
        self.類型 = "立即施放"

class 火球術:
    def __init__(self):
        super().__init__()
        self.名稱="火球術"
        self.描述= "對敵方單體造成([魔法]攻擊x0.3+[火屬性攻擊]x0.4點)傷害，並給予[火屬性攻擊]x0.4層[燃燒]效果，持續三回合。"
        self.消耗魔力=0
        self.當前冷卻=0
        self.冷卻回合數=2
        self.類型 = "敵方單體"
        
#============================

class 憤怒蘑菇:
    def __init__(self):
        super().__init__()
        self.名稱="憤怒蘑菇"
        self.描述="於戰鬥中使用，消耗5%生命，獲得[憤怒]狀態提升15%攻擊力，持續一回合，使用該物品不會消耗行動。"
        self.消耗魔力=0
        self.當前冷卻=0
        self.冷卻回合數=0
        self.類型 = "立即施放"


        
#



class 蔓延:
    def __init__(self):
        super().__init__()
        self.名稱="蔓延"
        self.描述= "對敵方單體造成(火屬性攻擊x1.2)火系傷害，若目標帶有燃燒效果，則會使戰場敵我全體皆獲得等同一半的燃燒效果。"
        self.消耗魔力=0
        self.當前冷卻=0
        self.冷卻回合數=5
        self.類型 = "敵方單體"

class 電火花:
    def __init__(self):
        super().__init__()
        self.名稱="電火花"
        self.描述="給予自己兩顆初級雷骰持續2回合。"
        self.消耗魔力=0
        self.當前冷卻=0
        self.冷卻回合數=2
        self.類型 = "立即施放"

class 電球術:
    def __init__(self):
        super().__init__()
        self.名稱="電球術"
        self.描述="對隨機敵人造成(魔法攻擊x0.2+雷屬性攻擊x0.2)傷害，有機率爆擊，該行動進行4次。"
        self.消耗魔力=0
        self.當前冷卻=0
        self.冷卻回合數=2
        self.類型 = "立即施放"

class 麻痺術:
    def __init__(self):
        super().__init__()
        self.名稱="麻痺術"
        self.描述= "對敵方單體造成(雷屬性攻擊x1.5)雷系傷害，有機率爆擊，並給予15層麻痺效果，持續2回合。麻痺:使其更容易被爆擊"
        self.消耗魔力=0
        self.當前冷卻=0
        self.冷卻回合數=4
        self.類型 = "敵方單體"

class 落石:
    def __init__(self):
        super().__init__()
        self.名稱="落石"
        self.描述= "給予敵人3顆落石骰，持續兩回合，若其擲到落石則會受到(魔法攻擊*0.3+地屬性攻擊*0.3)地系魔法傷害。"
        self.消耗魔力=0
        self.當前冷卻=0
        self.冷卻回合數=2
        self.類型 = "敵方單體"

class 石化術:
    def __init__(self):
        super().__init__()
        self.名稱="石化術"
        self.描述= "使目標速度-4,物理防禦+12，持續3回合。"
        self.消耗魔力=0
        self.當前冷卻=0
        self.冷卻回合數=3
        self.類型 = "單體"

class 小型地震:
    def __init__(self):
        super().__init__()
        self.名稱="小型地震"
        self.描述= "使敵方單體重骰骰子，並於其後對其造成(魔法攻擊x0.8+地屬性攻擊x0.6)點傷害。"
        self.消耗魔力=0
        self.當前冷卻=0
        self.冷卻回合數=4
        self.類型 = "敵方單體"
 



class 影襲:
    def __init__(self):
        super().__init__()
        self.名稱="影襲"
        self.描述= "對單個敵人進行攻擊造成(物理攻擊x0.6+暗屬性攻擊x0.6)，並額外獲得一次行動機會。"
        self.消耗魔力=0
        self.當前冷卻=0
        self.冷卻回合數=2
        self.類型 = "敵方單體"
    
class 怒吼:
    def __init__(self):
        super().__init__()
        self.名稱="怒吼"
        self.描述="怒吼，使自己及隊友獲得物理攻擊+4，持續2回合。"
        self.消耗魔力=0
        self.當前冷卻=0
        self.冷卻回合數=4
        self.類型 = "立即施放"

#雪人
class 霜寒吐息:
    def __init__(self):
        super().__init__()
        self.名稱="霜寒吐息"
        self.描述= "對單個敵人吐出霜寒吐息，附加3層冰緩效果，持續15回合，若其速度低於-30則使其死亡。"
        self.消耗魔力=30
        self.當前冷卻=0
        self.冷卻回合數=2
        self.類型 = "敵方單體"
